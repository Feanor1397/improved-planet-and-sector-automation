
automate_farming_hive_planet = {
	available = {
		has_designation = col_farming
		owner = { has_authority = auth_hive_mind }
		free_jobs < 3
	}

	prio_districts = {
		district_farming
	}

	buildings = {
		1 = {
			building = building_hive_capital
			available = {
				always = yes
			}
		}

	#	2 = {
	#		building = building_spawning_pool
	#		available = {
	#			AND = {
	#				owner = { spawning_pool_upkeep_affordable = yes }
	#				owner = { has_policy_flag = ipa_allow_growth_buildings }
	#			}
	#		}
	#	}

		2 = {
			building = building_food_processing_facility
			available = {
				owner = {
					food_processing_facility_upkeep_affordable = yes
				}
			}
		}

		3 = {
			building = building_hive_node
			available = {
				owner = {
					hive_node_upkeep_affordable = yes
				}
			}
		}

		4 = {
			building = building_chemical_plant
			available = {
				owner = {
					chemical_plant_upkeep_affordable = yes
				}
			}
		}

		5 = {
			building = building_refinery
			available = {
				owner = {
					refinery_upkeep_affordable = yes
				}
			}
		}

		6 = {
			building = building_crystal_plant
			available = {
				owner = {
					crystal_plant_upkeep_affordable = yes
				}
			}
		}

		7 = {
			building = building_refinery
			available = {
				owner = {
					refinery_upkeep_affordable = yes
				}
			}
		}

		8 = {
			building = building_refinery
			available = {
				owner = {
					refinery_upkeep_affordable = yes
				}
			}
		}

		9 = {
			building = building_energy_grid
			available = {
				owner = {
					energy_grid_upkeep_affordable = yes
				}
			}
		}
	}
}