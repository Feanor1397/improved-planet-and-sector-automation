package ipa.translations

import ipa.ModInfo
import ipa.Policies

class TranslationChineseSimple :
    TranslationEnglish("localisation/simp_chinese/ipa_l_simp_chinese.yml", "l_simp_chinese") {

    override fun addMenuIncomes() {
        super.addMenuIncomes()

        addMenuIncomeTranslation("energy_minimum", "£energy£ §B最少 §!能量币收入", Policies.incomeSteps)
        addMenuIncomeTranslation("minerals_minimum", "£minerals£ §B最少 §!矿物收入", Policies.incomeSteps)
        addMenuIncomeTranslation("food_minimum", "£food£ §B最少 §!食物收入", Policies.incomeSteps)
        addMenuIncomeTranslation("food_desired", "£food£ §E预期的 §!食物收入", Policies.incomeSteps)
        addMenuIncomeTranslation("alloys_minimum", "£alloys£ §B最少 §!合金收入", Policies.incomeSteps)
        addMenuIncomeTranslation("alloys_desired", "£alloys£ §E预期的 §!合金收入", Policies.incomeSteps)
        addMenuIncomeTranslation(
            "consumer_goods_minimum",
            "£consumer_goods£ §B最少 §!消费品收入",
            Policies.incomeSteps
        )
        addMenuIncomeTranslation(
            "consumer_goods_desired",
            "£consumer_goods£ §E预期的 §!消费品收入",
            Policies.incomeSteps
        )
        addMenuIncomeTranslation(
            "strategic_minimum",
            "£volatile_motes£ £exotic_gases£ £rare_crystals£ §B最低 §!战略物资收入",
            Policies.incomeStepsStrategic
        )
        addMenuIncomeTranslation(
            "strategic_desired",
            "£volatile_motes£ £exotic_gases£ £rare_crystals£ §E预期的 §!战略物资收入",
            Policies.incomeStepsStrategic
        )
    }

    override fun addMenuIncomeTranslation(eventKey: String, dialogLabel: String, steps: Array<Int>) {
        super.addMenuIncomeTranslation(eventKey, dialogLabel, steps)

        // Main Menu
        add("ipa_settings_main.1.$eventKey.disabled", "$dialogLabel: §R禁用")
        for (step in steps) {
            add("ipa_settings_main.1.$eventKey.step$step", "$dialogLabel: §G$step")
        }
        add("ipa_settings_main.1.$eventKey.unlimited", "$dialogLabel: §R无限")

        // Sub Menu
        add("ipa_settings_$eventKey.1.title", dialogLabel)
        add("ipa_settings_$eventKey.1.desc", "")
        add("ipa_settings_$eventKey.1.disabled", "$dialogLabel: §R禁用")
        add("ipa_settings_$eventKey.1.unlimited", "$dialogLabel: §R无限")
        for (step in steps) {
            add("ipa_settings_$eventKey.1.step$step", "$dialogLabel: §G$step")
        }
        add("ipa_settings_$eventKey.1.back", "§R返回")
    }

    override fun addVarious() {
        super.addVarious()

        // Sector Types
        add("st_designated", "定向专精")
        add("st_designated_desc", "该星区内的所有殖民地都将依照选定的行星发展方向进行相应的发展。")
        add("st_mixed", "混合发展")
        add(
            "st_mixed_desc",
            "该星区将对星区内的所有殖民地进行规划发展，但并不依照殖民地已设定的发展方向进行规划。由于失去了专业化奖励，此星区的殖民地的产出效率可能较低，但在早期游戏时可能会更加有利。"
        )
        add("st_strict", "严格遵守")
        add(
            "st_strict_desc",
            "该星区将严格遵循星区内所有殖民地已经选定的规划方向进行发展，这意味着行星区块将不会交叉建设。例如：若殖民地被设定为采矿星球，这个殖民地将不会建设任何发电站或农场。"
        )
        add("st_designated_wip", "定向专精(WIP)")
        add(
            "st_designated_wip_desc",
            "该星区发展指令仍包含正在开发中的可能或尚未完全打磨的功能。如果此指令引发了BUG，请进行反馈并考虑切换至常规“定向专精功能”。"
        )
    }

    override fun addMenuOptions() {
        super.addMenuOptions()

        // Edicts
        add("edict_ipa_settings_menu", "§BImproved Planet and Sector Automation （星区&行星自动托管增强 ）")

        // Settings Menu
        add("ipa_settings_main.1.title", "星区&行星自动托管增强: §Bv${ModInfo.Version}")
        add(
            "ipa_settings_main.1.desc",
            "星区&行星自动托管增强: §Bv${ModInfo.Version} §!Updated: ${ModInfo.UpdatedAt}"
        )
        add("ipa_settings_main.1.options", "§B设置选项")
        add("ipa_settings_main.1.close", "§R关闭")
        add("ipa_settings_options.1.title", "IPA: §BOptions")
        add("ipa_settings_options.1.desc", "")
        add("ipa_settings_options.1.back", "§R返回")
        add("ipa_settings_options.1.allow_growth_buildings", "设置：允许建造人口增长型建筑物")
        add("ipa_settings_options.1.upgrade_min_free_slots", "设置：允许建筑升级时拥有的最少空建筑格数量")
        add("ipa_settings_options.1.admin_cap", "设置：首都星球建筑物选项")
        add("ipa_settings_options.1.ipa_options_allow_upgrade_strongholds_yes", "允许升级要塞: §G允许")
        add("ipa_settings_options.1.ipa_options_allow_upgrade_strongholds_no", "允许升级要塞: §R不允许")
        add(
            "ipa_settings_options.1.ipa_options_allow_agri_farm_buildings_yes",
            "允许在农业星球上建造农场等建筑: §G允许"
        )
        add(
            "ipa_settings_options.1.ipa_options_allow_agri_farm_buildings_no",
            "允许在农业星球上建造农场等建筑: §R不允许"
        )
        add("ipa_settings_options.1.ipa_options_force_show_income_options_yes", "强制显示收入：§G确定")
        add("ipa_settings_options.1.ipa_options_force_show_income_options_no", "强制显示收入：§R取消")
        add("ipa_settings_options.1.ipa_options_disallow_unity_yes", "禁止建造影响力建筑: §G禁止建造")
        add("ipa_settings_options.1.ipa_options_disallow_unity_no", "禁止建造影响力建筑: §R不禁止")


        // Settings Submenu: growth buildings
        add("ipa_settings_options_allow_growth_buildings.1.title", "IPA：§B允许人口增长性建筑物")
        add("ipa_settings_options_allow_growth_buildings.1.desc", "")
        add("ipa_settings_options_allow_growth_buildings.1.clinic_allowed", "§G允许 §B基因诊所")
        add("ipa_settings_options_allow_growth_buildings.1.clinic_forbidden", "§R禁用 §B基因诊所")
        add("ipa_settings_options_allow_growth_buildings.1.hospital_allowed", "§G允许 §B细胞修复中心")
        add("ipa_settings_options_allow_growth_buildings.1.hospital_forbidden", "§R禁用 §B细胞修复中心")
        add("ipa_settings_options_allow_growth_buildings.1.robot_assembly_allowed", "§G允许 §B机器人组装工厂")
        add("ipa_settings_options_allow_growth_buildings.1.robot_assembly_forbidden", "§R禁用 §B机器人组装工厂")
        add("ipa_settings_options_allow_growth_buildings.1.clone_vats_allowed", "§G允许 §B克隆仓")
        add("ipa_settings_options_allow_growth_buildings.1.clone_vats_forbidden", "§R禁用 §B克隆仓")
        add(
            "ipa_settings_options_allow_growth_buildings.1.machine_assembly_allowed",
            "§G允许 §B机械组装星球"
        )
        add(
            "ipa_settings_options_allow_growth_buildings.1.machine_assembly_forbidden",
            "§R禁止 §B机械组装星球"
        )
        add(
            "ipa_settings_options_allow_growth_buildings.1.hive_spawning_pool_allowed",
            "§G允许 §B蜂巢生产池"
        )
        add(
            "ipa_settings_options_allow_growth_buildings.1.hive_spawning_pool_forbidden",
            "§R禁止 §B蜂巢生产池"
        )
        add(
            "ipa_settings_options_allow_growth_buildings.1.necroid_elevation_allowed",
            "§G允许 §B升格之室"
        )
        add(
            "ipa_settings_options_allow_growth_buildings.1.necroid_elevation_forbidden",
            "§R禁止 §B升格之室"
        )
        add("ipa_settings_options_allow_growth_buildings.1.back", "§R返回")


        // Settings Submenu: settings upgrade min free slots
        add(
            "ipa_settings_options_upgrade_min_free_slots.1.title",
            "IPA: §B建筑升级时最少的空建筑格数量"
        )
        add(
            "ipa_settings_options_upgrade_min_free_slots.1.desc",
            "在升级生产厂房（民用工厂，铸造厂，研究实验室）之前，需要一定数量的空闲建筑格。在没必要的情况下可以为了节省资源而选择不升级。但是当星球上没有多余的空闲建筑格时或人口需要更多岗位时，这些建筑物始终还是会升级。"
        )
        for (i in 0..10) {
            add("ipa_settings_options_upgrade_min_free_slots.1.option$i.current", "§G空闲出$i 个建筑格时")
            add("ipa_settings_options_upgrade_min_free_slots.1.option$i.change", "§B空闲出 $i 个建筑格时")
        }
        add("ipa_settings_options_upgrade_min_free_slots.1.back", "§R返回")


        // Settings Submenu: admin cap
        add("ipa_settings_options_admin_cap.1.title", "IPA: §B首都星球设置选项")
        add("ipa_settings_options_admin_cap.1.desc", "设置首都星球的建筑物发展规则")

        add("ipa_settings_options_admin_cap.1.option_enabled.current", "§G启用首都星球建筑物设置")
        add("ipa_settings_options_admin_cap.1.option_enabled.change", "§B启用首都星球建筑物设置")

        add("ipa_settings_options_admin_cap.1.option_designation.current", "§G启用仅已规划发展方向的殖民地")
        add("ipa_settings_options_admin_cap.1.option_designation.change", "§B启用仅已规划发展方向的殖民地")

        add("ipa_settings_options_admin_cap.1.option_disabled.current", "§G禁用首都建筑物设置")
        add("ipa_settings_options_admin_cap.1.option_disabled.change", "§B禁用首都建筑物设置")

        add("ipa_settings_options_admin_cap.1.back", "§R返回")
    }
}
