package ipa.translations

import ipa.ModInfo
import ipa.Policies
import ipa.blocks.TranslationFile

open class TranslationEnglish(target: String, language: String) : TranslationFile(target, language) {

    constructor() : this("localisation/english/ipa_l_english.yml", "l_english")

    init {
        addCommonTranslations()
    }

    fun addCommonTranslations() {
        addVarious()
        addMenuOptions()
        addMenuIncomes()
    }

    open fun addMenuIncomes() {
        addMenuIncomeTranslation("energy_minimum", "£energy£ §BMinimum §!Energy Income", Policies.incomeSteps)
        addMenuIncomeTranslation("minerals_minimum", "£minerals£ §BMinimum §!Minerals Income", Policies.incomeSteps)
        addMenuIncomeTranslation("food_minimum", "£food£ §BMinimum §!Food Income", Policies.incomeSteps)
        addMenuIncomeTranslation("food_desired", "£food£ §EDesired §!Food Income", Policies.incomeSteps)
        addMenuIncomeTranslation("alloys_minimum", "£alloys£ §BMinimum §!Alloys Income", Policies.incomeSteps)
        addMenuIncomeTranslation("alloys_desired", "£alloys£ §EDesired §!Alloys Income", Policies.incomeSteps)
        addMenuIncomeTranslation(
            "consumer_goods_minimum",
            "£consumer_goods£ §BMinimum §!Consumer Goods Income",
            Policies.incomeSteps
        )
        addMenuIncomeTranslation(
            "consumer_goods_desired",
            "£consumer_goods£ §EDesired §!Consumer Goods Income",
            Policies.incomeSteps
        )
        addMenuIncomeTranslation(
            "strategic_minimum",
            "£volatile_motes£ £exotic_gases£ £rare_crystals£ §BMinimum §!Strategic Income",
            Policies.incomeStepsStrategic
        )
        addMenuIncomeTranslation(
            "strategic_desired",
            "£volatile_motes£ £exotic_gases£ £rare_crystals£ §EDesired §!Strategic Income",
            Policies.incomeStepsStrategic
        )
    }

    open fun addMenuIncomeTranslation(eventKey: String, dialogLabel: String, steps: Array<Int>) {
        // Main Menu
        add("ipa_settings_main.1.$eventKey.disabled", "$dialogLabel: §RDisabled")
        for (step in steps) {
            add("ipa_settings_main.1.$eventKey.step$step", "$dialogLabel: §G$step")
        }
        add("ipa_settings_main.1.$eventKey.unlimited", "$dialogLabel: §RUnlimited")

        // Sub Menu
        add("ipa_settings_$eventKey.1.title", dialogLabel)
        add("ipa_settings_$eventKey.1.desc", "")
        add("ipa_settings_$eventKey.1.disabled", "$dialogLabel: §RDisabled")
        add("ipa_settings_$eventKey.1.unlimited", "$dialogLabel: §RUnlimited")
        for (step in steps) {
            add("ipa_settings_$eventKey.1.step$step", "$dialogLabel: §G$step")
        }
        add("ipa_settings_$eventKey.1.back", "§RBack")
    }

    open fun addVarious() {
        // Sector Types
        add("st_designated", "Designated Focus")
        add("st_designated_desc", "The Sector will focus on planet designations and develop them accordingly.")
        add("st_mixed", "Mixed Focus")
        add(
            "st_mixed_desc",
            "The Sector will build on all planets, regardless of planet designations. This is inherently less efficient due to the loss of specialization bonuses but may be advantageous in the early game."
        )
        add("st_strict", "Strict Focus")
        add(
            "st_strict_desc",
            "The Sector will focus on planet designations but will never cross-build mining/farming/generator districts. If a planet is designated as a mining planet it will not build generator or farming districts."
        )
        add("st_designated_wip", "Designated Focus (WIP)")
        add(
            "st_designated_wip_desc",
            "Includes work-in-progress features and changes that may not be fully ready yet. If this designation is causing problems please report them and consider switching to regular designated focus."
        )
    }

    open fun addMenuOptions() {
        // Edicts
        add("edict_ipa_settings_menu", "§BImproved Planet & Sector Automation")

        // Settings Menu
        "ipa_settings_main".let { prefix ->
            add("$prefix.1.title", "Improved Planet and Sector Automation: §Bv${ModInfo.Version}")
            add(
                "$prefix.1.desc",
                "Improved Planet and Sector Automation: §Bv${ModInfo.Version} §!Updated: ${ModInfo.UpdatedAt}"
            )
            add("$prefix.1.options", "§BOptions")
            add("$prefix.1.close", "§RClose")
        }

        "ipa_settings_options".let { prefix ->
            add("$prefix.1.title", "§BOptions")
            add("$prefix.1.desc", "")
            add("$prefix.1.back", "§RBack")
            add("$prefix.1.allow_growth_buildings", "Configure: Growth buildings")
            add("$prefix.1.upgrade_min_free_slots", "Configure: Building upgrade min free slots")
            add("$prefix.1.pre_build_jobs", "Configure: Pre-build jobs")
            add("$prefix.1.admin_cap", "Configure: Admin cap buildings")
            add("$prefix.1.ipa_options_allow_upgrade_strongholds_yes", "Upgrade Strongholds: §GYes")
            add("$prefix.1.ipa_options_allow_upgrade_strongholds_no", "Upgrade Strongholds: §RNo")
            add("$prefix.1.ipa_options_allow_agri_farm_buildings_yes", "Farm Building on Agri Designation: §GYes")
            add("$prefix.1.ipa_options_allow_agri_farm_buildings_no", "Farm Building on Agri Designation: §RNo")
            add("$prefix.1.ipa_options_force_show_income_options_yes", "Force Show Income Options: §GYes")
            add("$prefix.1.ipa_options_force_show_income_options_no", "Force Show Income Options: §RNo")
            add("$prefix.1.ipa_options_disallow_unity_yes", "Disallow unity buildings: §GYes")
            add("$prefix.1.ipa_options_disallow_unity_no", "Disallow unity buildings: §RNo")
        }


        // Settings Submenu: growth buildings
        "ipa_settings_options_allow_growth_buildings".let { prefix ->
            add("$prefix.1.title", "§BAllow growth buildings")
            add("$prefix.1.desc", "")
            add("$prefix.1.clinic_allowed", "§GAllow §BOrganic Clinic")
            add("$prefix.1.clinic_forbidden", "§RForbid §BOrganic Clinic")
            add("$prefix.1.hospital_allowed", "§GAllow §BHospital")
            add("$prefix.1.hospital_forbidden", "§RForbid §BHospital")
            add("$prefix.1.robot_assembly_allowed", "§GAllow §BRobot Assembly")
            add("$prefix.1.robot_assembly_forbidden", "§RForbid §BRobot Assembly")
            add("$prefix.1.clone_vats_allowed", "§GAllow §BClone Vats")
            add("$prefix.1.clone_vats_forbidden", "§RForbid §BClone Vats")
            add("$prefix.1.machine_assembly_allowed", "§GAllow §BMachine Assembly Planet")
            add("$prefix.1.machine_assembly_forbidden", "§RForbid §BMachine Assembly Planet")
            add("$prefix.1.hive_spawning_pool_allowed", "§GAllow §BHive Spawning Pool")
            add("$prefix.1.hive_spawning_pool_forbidden", "§RForbid §BHive Spawning Pool")
            add("$prefix.1.necroid_elevation_allowed", "§GAllow §BNecroid Elevation")
            add("$prefix.1.necroid_elevation_forbidden", "§RForbid §BNecroid Elevation")
            add("$prefix.1.back", "§RBack")
        }


        // Settings Submenu: settings upgrade min free slots
        "ipa_settings_options_upgrade_min_free_slots".let { prefix ->
            add("$prefix.1.title", "§BBuilding upgrade min free slots")
            add(
                "$prefix.1.desc",
                "Require a specified amount of free slots before upgrading production buildings (factory, foundry, research labs). This conserves strategic resources while there is no need to expend them. Buildings will always be upgraded if there are no more free slots available on the planet and more jobs are required."
            )
            for (i in 0..10) {
                add("$prefix.1.option$i.current", "§GRequire $i free slots")
                add("$prefix.1.option$i.change", "§BRequire $i free slots")
            }
            add("$prefix.1.back", "§RBack")
        }


        // Settings Submenu: pre-build jobs
        "ipa_settings_options_pre_build_jobs".let { prefix ->
            add(
                "$prefix.1.title", "§Bre-build jobs"
            )
            add(
                "$prefix.1.desc",
                "§REXPERIMENTAL: §BAttempt to pre-build a specified amount of jobs. This setting can be used in growth/resettlement heavy empires or when assimilating and transferring pods to your worlds from conquest to better cope with new pops and reduce the time spent in unemployment. \n0 pre-build jobs will result in new jobs being created once a unemployed pop is present on a planet. \n1 pre-build job will result in the automation trying to maintain at least 1 free job."
            )
            for (i in 0..2) {
                add("$prefix.1.option$i.current", "§GPre-build $i jobs")
                add("$prefix.1.option$i.change", "§BPre-build $i jobs")
            }
            add("$prefix.1.back", "§RBack")
        }

        // Settings Submenu: admin cap
        "ipa_settings_options_admin_cap".let { prefix ->
            add("$prefix.1.title", "§BAdmin Cap Options")
            add("$prefix.1.desc", "Set admin cap building rules.")

            add("$prefix.1.option_enabled.current", "§GEnabled admin cap buildings")
            add("$prefix.1.option_enabled.change", "§BEnable admin cap buildings")

            add("$prefix.1.option_designation.current", "§GEnabled only on designated colonies")
            add("$prefix.1.option_designation.change", "§BEnable only on designated colonies")

            add("$prefix.1.option_disabled.current", "§GDisabled admin cap buildings")
            add("$prefix.1.option_disabled.change", "§BDisable admin cap buildings")

            add("$prefix.1.back", "§RBack")
        }
    }

}
