package ipa.events

import ipa.Variables
import ipa.blocks.TemplateFile
import ipa.blocks.logic.Comparators
import ipa.blocks.policies.CountryEvent

open class IpaSettingsIncomeGeneric(
    eventKey: String,
    variable: Variables,
    steps: Array<Int>,
    allowUnlimited: Boolean = false,
    cb: (IpaSettingsIncomeGeneric.() -> Unit)? = null
) : TemplateFile("events/$eventKey.txt") {

    init {

        namespace = eventKey

        // Menu Event
        addBlock(CountryEvent("$eventKey.1")) {
            title = "$eventId.title"
            desc = "$eventId.desc"

            // Disable Option
            option("disabled") {
                hiddenEffect {
                    setVariable(variable, -10000)
                    // go back to main menu
                    countryEvent("ipa_settings_main.1")
                }
            }

            // Income Options
            for (stepIndex in steps.indices) {
                val step = steps[stepIndex]

                val stepLowerBound = steps.getOrNull(stepIndex - 6)
                val stepUpperBound = steps.getOrNull(stepIndex + 6)

                option("step$step") {

                    // show smaller steps only when selected income <= 1000
                    if (step != 0) {
                        if (stepLowerBound != null) {
                            trigger {
                                checkVariable(variable, Comparators.GTEQ, stepLowerBound)
                            }
                        }
                        // show larger steps only when selected income >= 500
                        if (stepUpperBound != null) {
                            trigger {
                                checkVariable(variable, Comparators.LTEQ, stepUpperBound)
                            }
                        }
                    }

                    hiddenEffect {
                        setVariable(variable, step)
                        // go back to main menu
                        countryEvent("ipa_settings_main.1")
                    }
                }
            }

            // Unlimited Option
            if (allowUnlimited) {
                option("unlimited") {
                    hiddenEffect {
                        setVariable(variable, 10000)
                        // go back to main menu
                        countryEvent("ipa_settings_main.1")
                    }
                }
            }

            // Back Option
            option("back") {
                hiddenEffect {
                    // go back to main menu
                    countryEvent("ipa_settings_main.1")
                }
            }

        }

        if (cb != null) {
            this.cb()
        }

    }
}
