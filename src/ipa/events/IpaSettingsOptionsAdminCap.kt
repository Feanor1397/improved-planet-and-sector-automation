package ipa.events

import ipa.Variables
import ipa.blocks.TemplateFile
import ipa.blocks.logic.Comparators
import ipa.blocks.policies.CountryEvent

class IpaSettingsOptionsAdminCap : TemplateFile("events/ipa_settings_options_admin_cap.txt", {
    namespace = "ipa_settings_options_admin_cap"

    // Main Menu
    addBlock(CountryEvent("$namespace.1")) {
        title = "$eventId.title"
        desc = "$eventId.desc"

        val options = mapOf("enabled" to 0, "designation" to 1, "disabled" to 2)

        for ((optKey, optValue) in options) {

            // selected option variant
            option("option_$optKey.current") {
                trigger {
                    checkVariable(Variables.OptionsManageAdminCap, Comparators.EQ, optValue)
                }

                hiddenEffect {
                    setVariable(Variables.OptionsManageAdminCap, optValue)
                    countryEvent("$namespace.1") // show dialog again
                }
            }

            // not selected option variant
            option("option_$optKey.change") {
                trigger {
                    NOT {
                        checkVariable(Variables.OptionsManageAdminCap, Comparators.EQ, optValue)
                    }
                }

                hiddenEffect {
                    countryEvent("$namespace.1") // show dialog again
                    setVariable(Variables.OptionsManageAdminCap, optValue)
                }
            }

        }

        // Back Option
        option("back") {
            hiddenEffect {
                countryEvent("ipa_settings_options.1")
            }
        }

    }

})
