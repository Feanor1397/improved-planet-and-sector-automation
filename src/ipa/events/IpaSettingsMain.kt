package ipa.events

import ipa.Policies
import ipa.Variables
import ipa.blocks.TemplateFile
import ipa.blocks.logic.Comparators
import ipa.blocks.policies.CountryEvent

fun CountryEvent.addIncomeStepOptions(
    eventKey: String,
    variable: Variables,
    steps: Array<Int>,
    usesFood: Boolean? = null,
    usesConsumerGoods: Boolean? = null
) {

    // disabled
    option("$eventKey.disabled") {
        trigger {
            AND {
                checkVariable(variable, Comparators.LTEQ, -10000)
                if (usesFood != null || usesConsumerGoods != null) {
                    OR {
                        checkVariable(Variables.OptionsForceShowIncomeOptions, Comparators.EQ, 1)
                        this.usesFood = usesFood
                        this.usesConsumerGoods = usesConsumerGoods
                    }
                }
            }
        }
        hiddenEffect {
            countryEvent("ipa_settings_$eventKey.1")
        }
    }


    // steps
    for (step in steps) {
        option("$eventKey.step$step") {
            trigger {
                AND {
                    checkVariable(variable, Comparators.EQ, step)
                    if (usesFood != null || usesConsumerGoods != null) {
                        OR {
                            checkVariable(Variables.OptionsForceShowIncomeOptions, Comparators.EQ, 1)
                            this.usesFood = usesFood
                            this.usesConsumerGoods = usesConsumerGoods
                        }
                    }
                }
            }
            hiddenEffect {
                countryEvent("ipa_settings_$eventKey.1")
            }
        }
    }

    // unlimited
    option("$eventKey.unlimited") {
        trigger {
            AND {
                checkVariable(variable, Comparators.GTEQ, 10000)
                if (usesFood != null || usesConsumerGoods != null) {
                    OR {
                        checkVariable(Variables.OptionsForceShowIncomeOptions, Comparators.EQ, 1)
                        this.usesFood = usesFood
                        this.usesConsumerGoods = usesConsumerGoods
                    }
                }
            }
        }
        hiddenEffect {
            countryEvent("ipa_settings_$eventKey.1")
        }
    }
}

class IpaSettingsMain : TemplateFile("events/ipa_settings_main.txt", {
    namespace = "ipa_settings_main"

    // Main Menu
    addBlock(CountryEvent("ipa_settings_main.1")) {
        title = "$eventId.title"
        desc = "$eventId.desc"

        addIncomeStepOptions("energy_minimum", Variables.IncomeEnergyMinimum, Policies.incomeSteps)
        addIncomeStepOptions("minerals_minimum", Variables.IncomeMineralsMinimum, Policies.incomeSteps)
        addIncomeStepOptions("food_minimum", Variables.IncomeFoodMinimum, Policies.incomeSteps, usesFood = true)
        addIncomeStepOptions("food_desired", Variables.IncomeFoodDesired, Policies.incomeSteps, usesFood = true)
        // addIncomeStepOptions("alloys_minimum", Variables.IncomeAlloysMinimum, Policies.incomeSteps)
        addIncomeStepOptions("alloys_desired", Variables.IncomeAlloysDesired, Policies.incomeSteps)
        addIncomeStepOptions(
            "consumer_goods_minimum",
            Variables.IncomeConsumerGoodsMinimum,
            Policies.incomeSteps,
            usesConsumerGoods = true
        )
        addIncomeStepOptions(
            "consumer_goods_desired",
            Variables.IncomeConsumerGoodsDesired,
            Policies.incomeSteps,
            usesConsumerGoods = true
        )
        addIncomeStepOptions("strategic_minimum", Variables.IncomeStrategicMinimum, Policies.incomeStepsStrategic)
        addIncomeStepOptions("strategic_desired", Variables.IncomeStrategicDesired, Policies.incomeStepsStrategic)

        // Options
        option("options") {
            hiddenEffect {
                countryEvent("ipa_settings_options.1")
            }
        }

        // Close Menu
        option("close") {
            // close menu, no-op
        }
    }

})
