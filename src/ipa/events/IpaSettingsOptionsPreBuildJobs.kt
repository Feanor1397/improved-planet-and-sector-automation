package ipa.events

import ipa.Variables
import ipa.blocks.TemplateFile
import ipa.blocks.logic.Comparators
import ipa.blocks.policies.CountryEvent

class IpaSettingsOptionsPreBuildJobs : TemplateFile("events/ipa_settings_options_pre_build_jobs.txt", {
    namespace = "ipa_settings_options_pre_build_jobs"

    // Main Menu
    addBlock(CountryEvent("$namespace.1")) {
        title = "$eventId.title"
        desc = "$eventId.desc"

        for (i in 0..2) {

            // selected option variant
            option("option$i.current") {
                trigger {
                    checkVariable(Variables.OptionsPreBuildJobs, Comparators.EQ, i)
                }

                hiddenEffect {
                    setVariable(Variables.OptionsPreBuildJobs, i)
                    countryEvent("$namespace.1") // show dialog again
                }
            }

            // not selected option variant
            option("option$i.change") {
                trigger {
                    NOT {
                        checkVariable(Variables.OptionsPreBuildJobs, Comparators.EQ, i)
                    }
                }

                hiddenEffect {
                    countryEvent("$namespace.1") // show dialog again
                    setVariable(Variables.OptionsPreBuildJobs, i)
                }
            }

        }

        // Back Option
        option("back") {
            hiddenEffect {
                countryEvent("ipa_settings_options.1")
            }
        }

    }

})
