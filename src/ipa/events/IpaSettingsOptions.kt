package ipa.events

import ipa.Variables
import ipa.blocks.TemplateFile
import ipa.blocks.policies.CountryEvent

class IpaSettingsOptions : TemplateFile("events/ipa_settings_options.txt", {
    namespace = "ipa_settings_options"

    // Main Menu
    addBlock(CountryEvent("ipa_settings_options.1")) {
        title = "$eventId.title"
        desc = "$eventId.desc"

        option("allow_growth_buildings") {
            hiddenEffect {
                countryEvent("ipa_settings_options_allow_growth_buildings.1")
            }
        }

        option("upgrade_min_free_slots") {
            hiddenEffect {
                countryEvent("ipa_settings_options_upgrade_min_free_slots.1")
            }
        }

        option("pre_build_jobs") {
            hiddenEffect {
                countryEvent("ipa_settings_options_pre_build_jobs.1")
            }
        }

        option("admin_cap") {
            hiddenEffect {
                countryEvent("ipa_settings_options_admin_cap.1")
            }
        }

        addToggleOption(Variables.OptionsAllowUpgradeStronghold)
        addToggleOption(Variables.OptionsAllowAgriFarmBuildings)
        addToggleOption(Variables.OptionsForceShowIncomeOptions)
        addToggleOption(Variables.OptionsDisallowUnity)

        // Back Option
        option("back") {
            hiddenEffect {
                // go back to main menu
                countryEvent("ipa_settings_main.1")
            }
        }

    }

})
