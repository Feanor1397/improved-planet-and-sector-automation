package ipa

import ipa.blocks.TemplateContext
import ipa.blocks.TemplateFile
import ipa.events.*
import ipa.scriptedtriggers.*
import ipa.translations.*
import ipa.sectorfocus.Basic as SectorFocusBasic
import ipa.sectorfocus.Designated as SectorFocusDesignated
import ipa.sectorfocus.Mixed as SectorFocusMixed
import ipa.sectorfocus.Strict as SectorFocusStrict

object Assemble {

    @JvmStatic
    fun main(args: Array<String>) {
        val ctx = TemplateContext("./mod", "./doc")

        ctx.addFile(SectorFocusBasic())
        ctx.addFile(SectorFocusDesignated())
        ctx.addFile(SectorFocusMixed())
        ctx.addFile(SectorFocusStrict())
        // ctx.addFile(SectorFocusDesignatedWIP())

        ctx.addFile(TemplateFile("common/scripted_triggers/ipa_designations.txt") {
            addBlock(IpaHasDesignationCapital())
            addBlock(IpaHasDesignationGenerator())
            addBlock(IpaHasDesignationMining())
            addBlock(IpaHasDesignationFarming())
            addBlock(IpaHasDesignationResearch())
            addBlock(IpaHasDesignationLeisure())
            addBlock(IpaHasDesignationTrade())
            addBlock(IpaHasDesignationUrban())
            addBlock(IpaHasDesignationFactory())
            addBlock(IpaHasDesignationRefinery())
            addBlock(IpaHasDesignationFoundry())
            addBlock(IpaHasDesignationIndustrial())
            addBlock(IpaHasDesignationFortress())
            addBlock(IpaHasDesignationRural())
            addBlock(IpaHasDesignationPenal())
            addBlock(IpaHasDesignationResort())
            addBlock(IpaHasDesignationMix())
            addBlock(IpaHasDesignationBureau())
        })

        ctx.addFile(TemplateFile("common/scripted_triggers/ipa_income.txt") {
            addBlock(IpaMissingDesiredIncomeAlloys())
            addBlock(IpaMissingDesiredIncomeConsumerGoods())
            addBlock(IpaMissingDesiredIncomeExoticGases())
            addBlock(IpaMissingDesiredIncomeFood())
            addBlock(IpaMissingDesiredIncomeRareCrystals())
            addBlock(IpaMissingDesiredIncomeVolatileMotes())
            addBlock(IpaMissingIncomeAlloys())
            addBlock(IpaMissingIncomeConsumerGoods())
            addBlock(IpaMissingIncomeEnergy())
            addBlock(IpaMissingIncomeExoticGases())
            addBlock(IpaMissingIncomeFood())
            addBlock(IpaMissingIncomeMinerals())
            addBlock(IpaMissingIncomeRareCrystals())
            addBlock(IpaMissingIncomeVolatileMotes())
        })

        ctx.addFile(TemplateFile("common/scripted_triggers/ipa_options.txt") {
            addBlock(IpaAllowGrowthClinic())
            addBlock(IpaAllowGrowthHospital())
            addBlock(IpaAllowGrowthRobotAssembly())
            addBlock(IpaAllowGrowthCloneVats())
            addBlock(IpaAllowGrowthMachineAssembly())
            addBlock(IpaAllowGrowthHiveSpawningPool())
            addBlock(IpaAllowGrowthNecroidElevation())
            addBlock(IpaUpgradeMinFreeSlots())
            addBlock(IpaAdminCapAllowed())
            addBlock(IpaUnityAllowed())
            addBlock(IpaNeedMoreJobsCheck())
        })

        ctx.addFile(TemplateFile("common/scripted_triggers/ipa_country.txt") {
            addBlock(IpaCountryUsesFood())
        })

        // events
        ctx.addFile(IpaSettingsMain())
        ctx.addFile(IpaSettingsEnergyMinimum())
        ctx.addFile(IpaSettingsMineralsMinimum())
        ctx.addFile(IpaSettingsFoodMinimum())
        ctx.addFile(IpaSettingsFoodDesired())
        ctx.addFile(IpaSettingsAlloysMinimum())
        ctx.addFile(IpaSettingsAlloysDesired())
        ctx.addFile(IpaSettingsConsumerGoodsMinimum())
        ctx.addFile(IpaSettingsConsumerGoodsDesired())
        ctx.addFile(IpaSettingsStrategicMinimum())
        ctx.addFile(IpaSettingsStrategicDesired())
        ctx.addFile(IpaSettingsOptions())
        ctx.addFile(IpaSettingsOptionsAllowGrowthBuildings())
        ctx.addFile(IpaSettingsOptionsUpgradeMinFreeSlots())
        ctx.addFile(IpaSettingsOptionsAdminCap())
        ctx.addFile(IpaSettingsOptionsPreBuildJobs())

        // translations
        ctx.addFile(TranslationEnglish())
        ctx.addFile(TranslationFrench())
        ctx.addFile(TranslationGerman())
        ctx.addFile(TranslationPolish())
        ctx.addFile(TranslationRussian())
        ctx.addFile(TranslationSpanish())
        ctx.addFile(TranslationChineseSimple())
        ctx.addFile(TranslationBrazPor())

        ctx.writeFiles()
    }

}
