package ipa.defines

enum class Resource(val code: String) {
    Minerals("minerals"),
    Energy("energy"),
    Food("food"),
    Alloys("alloys"),
    ConsumerGoods("consumer_goods"),
    RareCrystals("rare_crystals"),
    ExoticGases("exotic_gases"),
    VolatileMotes("volatile_motes"),
}
