package ipa.defines

enum class DistrictGroup(vararg val districts: District) : Iterable<District> {

    Housing(
        District.City,
        District.HabitatHousing,
        District.Hive,
        District.Nexus,
        District.RingworldCity,
        District.RingworldHive,
        District.RingworldNexus
    ),
    Mining(
        District.Mining,
        District.HabitatMining,
        District.MiningUncapped
    ),
    Generator(
        District.Generator,
        District.RingworldGenerator,
        District.HabitatEnergy,
        District.GeneratorUncapped
    ),
    Farming(
        District.Farming,
        District.RingworldFarming,
        District.FarmingUncapped,
    ),
    Research(
        District.RingworldResearch,
        District.HabitatResearch,
    ),
    Leisure(
        District.HabitatCultural,
        District.ArcologyLeisure,
    ),
    Trade(
        District.RingworldCommercial,
        District.HabitatCommercial,
    ),
    Industrial(
        District.Industrial,
        District.HabitatIndustrial,
        District.RingworldIndustrial,
    );

    override fun iterator(): Iterator<District> {
        return districts.iterator()
    }

}
