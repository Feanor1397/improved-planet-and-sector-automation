package ipa.defines

enum class BuildingGroup(vararg val buildings: Building) : Iterable<Building> {
    AdminCapT1(
        Building.bureaucratic_1,
        Building.uplink_node,
        Building.hive_node
    ),
    AdminCapT2(
        Building.bureaucratic_2,
        Building.network_junction,
        Building.hive_cluster
    ),
    AdminCapT3(
        // Building.bureaucratic_3, // building is currently not enabled in base game
        Building.system_conflux,
        Building.hive_confluence
    ),
    CrimeT1(
        Building.precinct_house,
        Building.sentinel_posts
    ),
    HousingBuildingsT1(
        Building.luxury_residence,
        Building.hive_warren,
        Building.drone_storage,
    ),
    HousingBuildingsT2(
        Building.paradise_dome,
        Building.expanded_warren,
        Building.drone_megastorage,
    ),

    AmenitiesBuildingsT1(
        Building.holo_theatres,
    ),
    AmenitiesBuildingsT2(
        Building.hyper_entertainment_forum,
    ),
    CapitalBuildings(
        Building.colony_shelter,
        Building.capital,
        Building.major_capital,
        Building.system_capital,
        Building.deployment_post,
        Building.machine_capital,
        Building.machine_major_capital,
        Building.machine_system_capital,
        Building.hive_capital,
        Building.hive_major_capital,
        Building.hab_capital,
        Building.hab_major_capital,
        Building.resort_capital,
        Building.slave_capital,
        Building.slave_major_capital,
    );

    override fun iterator(): Iterator<Building> {
        return buildings.iterator()
    }

}
