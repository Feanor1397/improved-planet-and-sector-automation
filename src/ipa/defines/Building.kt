package ipa.defines

enum class Building(val code: String) {
    // growth buildings
    spawning_pool("building_spawning_pool"),
    robot_assembly_plant("building_robot_assembly_plant"),
    machine_assembly_plant("building_machine_assembly_plant"),
    machine_assembly_complex("building_machine_assembly_complex"),
    clinic("building_clinic"),
    hospital("building_hospital"),
    clone_vats("building_clone_vats"),
    necrophage_elevation_chamber("building_necrophage_elevation_chamber"),
    necrophage_house_of_apotheosis("building_necrophage_house_of_apotheosis"),

    // admin cap buildings
    bureaucratic_1("building_bureaucratic_1"),
    bureaucratic_2("building_bureaucratic_2"),
    bureaucratic_3("building_bureaucratic_3"),
    uplink_node("building_uplink_node"),
    hive_node("building_hive_node"),
    network_junction("building_network_junction"),
    hive_cluster("building_hive_cluster"),
    system_conflux("building_system_conflux"),
    hive_confluence("building_hive_confluence"),

    // crime buildings
    precinct_house("building_precinct_house"),
    sentinel_posts("building_sentinel_posts"),

    foundry_1("building_foundry_1"),
    foundry_2("building_foundry_2"),
    foundry_3("building_foundry_3"),

    factory_1("building_factory_1"),
    factory_2("building_factory_2"),
    factory_3("building_factory_3"),

    // Research
    supercomputer("building_supercomputer"),
    institute("building_institute"),
    research_lab_1("building_research_lab_1"),
    research_lab_2("building_research_lab_2"),
    research_lab_3("building_research_lab_3"),

    // Housing T1
    luxury_residence("building_luxury_residence"),
    hive_warren("building_hive_warren"),
    drone_storage("building_drone_storage"),

    // Housing T2
    paradise_dome("building_paradise_dome"),
    expanded_warren("building_expanded_warren"),
    drone_megastorage("building_drone_megastorage"),

    // amenities T1
    holo_theatres("building_holo_theatres"),

    // amenities T2
    hyper_entertainment_forum("building_hyper_entertainment_forum"),

    // Capitals
    colony_shelter("building_colony_shelter"),
    capital("building_capital"),
    major_capital("building_major_capital"),
    system_capital("building_system_capital"),
    deployment_post("building_deployment_post"),
    machine_capital("building_machine_capital"),
    machine_major_capital("building_machine_major_capital"),
    machine_system_capital("building_machine_system_capital"),
    hive_capital("building_hive_capital"),
    hive_major_capital("building_hive_major_capital"),
    hab_capital("building_hab_capital"),
    hab_major_capital("building_hab_major_capital"),
    resort_capital("building_resort_capital"),
    slave_capital("building_slave_capital"),
    slave_major_capital("building_slave_major_capital"),

    // production enhancers
    mineral_purification_plant("building_mineral_purification_plant"),
    mineral_purification_hub("building_mineral_purification_hub"),
    energy_grid("building_energy_grid"),
    energy_nexus("building_energy_nexus"),
    food_processing_facility("building_food_processing_facility"),
    food_processing_center("building_food_processing_center"),
    ministry_production("building_ministry_production"),
    production_center("building_production_center"),

    // unity
    simulation_1("building_simulation_1"),
    simulation_2("building_simulation_2"),
    simulation_3("building_simulation_3"),
    alpha_hub("building_alpha_hub"),
    autochthon_monument("building_autochthon_monument"),
    heritage_site("building_heritage_site"),
    hypercomms_forum("building_hypercomms_forum"),
    autocurating_vault("building_autocurating_vault"),
    temple("building_temple"),
    holotemple("building_holotemple"),
    sacred_nexus("building_sacred_nexus"),
    citadel_of_faith("building_citadel_of_faith"),

    // strategic
    crystal_mines("building_crystal_mines") ,
    gas_extractors("building_gas_extractors"),
    mote_harvesters("building_mote_harvesters"),
    chemical_plant("building_chemical_plant"),
    refinery("building_refinery"),
    crystal_plant("building_crystal_plant"),

    // military
    stronghold("building_stronghold"),
    fortress("building_fortress"),

    // misc
    hydroponics_farm("building_hydroponics_farm")
}
