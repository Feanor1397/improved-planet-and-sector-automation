package ipa.sectorfocus

import ipa.blocks.TemplateFile
import ipa.blocks.sector.SectorFocus
import ipa.sectorfocus.common.*

class DesignatedWIP : TemplateFile("common/sector_focuses/30_designated_wip.txt", {
    addBlock(SectorFocus("designated_wip")) {
        hidden = true

        addCommonHousing()
        addCommonUnity()
        addCommonGrowth()
        addCommonAdminCap()

        // research
        addCommonResearch()

        // district resource production
        addDistricts()

        // alloys
        addCommonFoundry()

        // consumer goods
        addCommonFactory()

        // farm buildings
        addCommonHydroponicsFarm()

        // strategic resources
        addCommonStrategicFactories()
        addCommonStrategicNatural()

        // special buildings
        addCommonProductionEnhancers()

        // special buildings
        addCommonMilitaryBuildings()

    }
})
