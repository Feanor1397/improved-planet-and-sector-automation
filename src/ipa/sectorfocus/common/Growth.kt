package ipa.sectorfocus.common

import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.Building
import ipa.defines.Resource

fun SectorFocus.addCommonGrowth(exemptFromJobsCheck: Boolean? = true, priority: Boolean? = true) {

    // hive
    addBuilding(
        Building.spawning_pool,
        priorities.population,
        priority = priority,
        exemptFromJobsCheck = exemptFromJobsCheck
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                addFoodIncomeBlock()
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control_gestalt")
        }
    }

    // robot non-gestalt
    addBuilding(
        Building.robot_assembly_plant,
        priorities.population,
        priority = priority,
        exemptFromJobsCheck = exemptFromJobsCheck
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_robot_assembly_control")
            hasModifier("planet_population_control_gestalt")
        }
    }

    // machine gestalt
    addBuilding(
        Building.machine_assembly_plant,
        priorities.population,
        priority = priority,
        exemptFromJobsCheck = exemptFromJobsCheck
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                hasMonthlyIncome(Resource.Alloys, Comparators.LTEQ, 10)
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_robot_assembly_control")
            hasModifier("planet_population_control_gestalt")
        }
    }
    addBuilding(
        Building.machine_assembly_complex,
        priorities.population + 1,
        priority = priority,
        exemptFromJobsCheck = exemptFromJobsCheck
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                hasMonthlyIncome(Resource.Alloys, Comparators.LTEQ, 10)
                missingIncomeRareCrystals = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_robot_assembly_control")
            hasModifier("planet_population_control_gestalt")
        }
    }

    // bio
    addBuilding(
        Building.clinic,
        priorities.population,
        priority = true,
        exemptFromJobsCheck = exemptFromJobsCheck
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                AND {
                    usesConsumerGoods = true
                    missingIncomeConsumerGoods = true
                }
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control")
            hasModifier("planet_population_control_gestalt")
        }
    }
    addBuilding(
        Building.hospital,
        priorities.population,
        priority = priority,
        exemptFromJobsCheck = exemptFromJobsCheck
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
                missingIncomeExoticGases = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control")
        }
    }
    addBuilding(
        Building.clone_vats,
        priorities.population,
        priority = priority,
        exemptFromJobsCheck = exemptFromJobsCheck
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeEnergy = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control")
        }
    }

    // necroids
    addBuilding(
        Building.necrophage_elevation_chamber,
        priorities.population,
        priority = priority,
        exemptFromJobsCheck = exemptFromJobsCheck
    ) {
        modifierOR(priorities.disable) {
            controller {
                addFoodIncomeBlock()
                missingIncomeEnergy = true
                missingIncomeFood = true
                missingIncomeConsumerGoods = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
        }
    }
    addBuilding(
        Building.necrophage_house_of_apotheosis,
        priorities.population + 1,
        priority = priority,
        exemptFromJobsCheck = exemptFromJobsCheck
    ) {
        modifierOR(priorities.disable) {
            controller {
                addFoodIncomeBlock()
                missingIncomeEnergy = true
                missingIncomeFood = true
                missingIncomeConsumerGoods = true
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
        }
    }

}
