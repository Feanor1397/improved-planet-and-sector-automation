package ipa.sectorfocus.common

import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.DistrictGroup

fun SectorFocus.addDistricts(
    strictDesignations: Boolean = false,
    habitatSpecialization: Boolean = true
) {

    for (district in DistrictGroup.Generator) {
        addDistrict(district, priorities.districts) {

            modifier(1.02) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    missingIncomeEnergy = true
                }
            }

            modifier(1.10) {
                comment = "increase priority for planets with matching focus" +
                    ", slightly stronger than missing income to ensure districts are build by designations first"
                hasDesignationGenerator()
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                if (strictDesignations) {
                    hasDesignationGenerator(false)
                } else if (habitatSpecialization) {
                    AND {
                        comment = "disallow on non-matching habitats as these need more strict specialization"
                        isPlanetClassHabitat()
                        hasDesignationGenerator(false)
                    }
                }

                AND {
                    comment = "Disallow districts when possible to build research"
                    hasDesignationResearch()
                    freeBuildingSlots(Comparators.GT, 0)
                }
            }
        }
    }

    for (district in DistrictGroup.Farming) {
        addDistrict(district, priorities.districts) {

            modifier(1.02) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    missingIncomeFood = true
                }
            }

            modifier(1.10) {
                comment = "increase priority for planets with matching focus" +
                    ", slightly stronger than missing income to ensure districts are build by designations first"
                hasDesignationFarming()
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                if (strictDesignations) {
                    hasDesignationFarming(false)
                } else if (habitatSpecialization) {
                    AND {
                        comment = "disallow on non-matching habitats as these need more strict specialization"
                        isPlanetClassHabitat()
                        hasDesignationFarming(false)
                    }
                }

                AND {
                    // TODO consider if this is a good choice, sometimes you might rather prefer the pops resettling to other worlds were they are actually useful instead of creating more than needed food
                    comment = "only disallow farming districts above desired income if not a agri world"
                    hasDesignationFarming(false)
                    controller {
                        missingDesiredIncomeFood = false
                    }
                }

                owner {
                    usesFood = false
                }

                AND {
                    comment = "Disallow districts when possible to build research"
                    hasDesignationResearch()
                    freeBuildingSlots(Comparators.GT, 0)
                }
            }
        }
    }

    for (district in DistrictGroup.Mining) {
        addDistrict(district, priorities.districts) {

            modifier(1.02) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    missingIncomeMinerals = true
                }
            }

            modifier(1.10) {
                comment = "increase priority for planets with matching focus" +
                    ", slightly stronger than missing income to ensure districts are build by designations first"
                hasDesignationMining()
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                if (strictDesignations) {
                    hasDesignationMining(false)
                } else if (habitatSpecialization) {
                    AND {
                        comment = "disallow on non-matching habitats as these need more strict specialization"
                        isPlanetClassHabitat()
                        hasDesignationMining(false)
                    }
                }

                AND {
                    comment = "Disallow districts when possible to build research"
                    hasDesignationResearch()
                    freeBuildingSlots(Comparators.GT, 0)
                }
            }
        }
    }

    for (district in DistrictGroup.Research) {
        addDistrict(district, priorities.districts) {

            modifier(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignationResearch()
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                if (strictDesignations) {
                    hasDesignationResearch(false)
                } else if (habitatSpecialization) {
                    AND {
                        comment = "disallow on non-matching habitats as these need more strict specialization"
                        isPlanetClassHabitat()
                        hasDesignationResearch(false)
                    }
                }

                controller {
                    missingIncomeEnergy = true
                    missingIncomeMinerals = true
                }

                addResearchRequirements()
            }
        }
    }

    for (district in DistrictGroup.Leisure) {
        addDistrict(district, priorities.districts) {

            modifier(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignationLeisure()
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                controller {
                    missingIncomeConsumerGoods = true
                    missingIncomeEnergy = true
                }

                hasDesignationLeisure(false)
            }
        }
    }

    for (district in DistrictGroup.Industrial) {
        addDistrict(district, priorities.industrial) {

            modifierOR(1.01) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    missingDesiredIncomeAlloys = true
                    missingIncomeConsumerGoods = true
                }
            }

            modifierOR(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignationIndustrialAny()
            }

            modifierOR(priorities.disable) {
                comment = "Disallow for various conditions"
                disableIfNoNeedForJobs()

                if (strictDesignations) {
                    notHasDesignationIndustrialAny()
                } else if (habitatSpecialization) {
                    AND {
                        comment = "disallow on non-matching habitats as these need more strict specialization"
                        isPlanetClassHabitat()
                        notHasDesignationIndustrialAny()
                    }
                }

                controller {
                    missingIncomeEnergy = true
                    missingIncomeMinerals = true
                }

                AND {
                    comment = "only check desired income on non-industrial designations"
                    notHasDesignationIndustrialAny() // do not prevent continued production on industrial worlds
                    controller {
                        OR {
                            AND {
                                comment = "disallow if both are above desired value"
                                usesConsumerGoods = true
                                missingDesiredIncomeAlloys = false
                                missingDesiredIncomeConsumerGoods = false
                            }
                            AND {
                                usesConsumerGoods = false
                                missingDesiredIncomeAlloys = false
                            }
                        }
                    }
                }

                // prevent on some specialized worlds by default
                hasDesignationResearch()
                hasDesignationPenal()
                hasDesignationResort()
            }
        }
    }

    for (district in DistrictGroup.Trade) {
        addDistrict(district, priorities.districts) {

            modifierOR(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignationTrade()
            }

            modifierOR(priorities.disable) {
                comment = "Disallow for various conditions"
                disableIfNoNeedForJobs()
                hasDesignationTrade(false)
            }
        }
    }

}
