package ipa.sectorfocus.common

import ipa.Variables
import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.Building

fun SectorFocus.addCommonHydroponicsFarm() {

    addBuilding(Building.hydroponics_farm, priorities.defaultReduced) {
        modifierOR(priorities.disable) {
            hasDesignationFarming(false)
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
            }
            owner {
                checkVariable(Variables.OptionsAllowAgriFarmBuildings, Comparators.EQ, 0)
            }
        }
    }

}
