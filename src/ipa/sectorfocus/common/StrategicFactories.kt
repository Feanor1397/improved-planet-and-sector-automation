package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus
import ipa.defines.Building

fun SectorFocus.addCommonStrategicFactories() {

    addBuilding(Building.chemical_plant, priorities.strategic, priority = true) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeVolatileMotes = true
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow if to many free jobs or above desired income"
            disableIfNoNeedForJobs()

            controller {
                missingIncomeMinerals = true
                missingDesiredIncomeVolatileMotes = false
            }

            // prevent on some specialized worlds by default
            hasDesignationResearch()
            hasDesignationPenal()
            hasDesignationResort()
        }
    }

    addBuilding(Building.refinery, priorities.strategic, priority = true) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeExoticGases = true
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()

            controller {
                missingIncomeMinerals = true
                missingDesiredIncomeExoticGases = false
            }

            // prevent on some specialized worlds by default
            hasDesignationResearch()
            hasDesignationPenal()
            hasDesignationResort()
        }
    }

    addBuilding(Building.crystal_plant, priorities.strategic, priority = true) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeRareCrystals = true
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()

            controller {
                missingIncomeMinerals = true
                missingDesiredIncomeRareCrystals = false
            }

            // prevent on some specialized worlds by default
            hasDesignationResearch()
            hasDesignationPenal()
            hasDesignationResort()
        }
    }

}
