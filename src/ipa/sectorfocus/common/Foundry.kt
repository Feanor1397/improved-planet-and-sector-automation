package ipa.sectorfocus.common

import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.Building
import ipa.defines.District
import ipa.defines.DistrictGroup

fun SectorFocus.addCommonFoundry() {

    addBuilding(Building.foundry_1, priorities.industrial) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingDesiredIncomeAlloys = true
            }
        }

        modifier(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignationResearch()
            hasDesignationRefinery()
            hasDesignationFactory()
            hasDesignationLeisure()
            hasDesignationResort()
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingDesiredIncomeAlloys = false
            }
            AND {
                numDistricts(District.Industrial, Comparators.LTEQ, 1)
            }
        }
    }

    addBuilding(Building.foundry_2, priorities.industrial) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingDesiredIncomeAlloys = true
            }
        }

        modifier(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingIncomeVolatileMotes = true
                // always upgrade to improve efficiency, even if not missing income
                // missingDesiredIncomeAlloys = false
            }
            /*AND {
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LTEQ, 1)
                }
            }*/
        }
    }

    addBuilding(Building.foundry_3, priorities.industrial) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingDesiredIncomeAlloys = true
            }
        }

        modifier(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingIncomeVolatileMotes = true
                // always upgrade to improve efficiency, even if not missing income
                // missingDesiredIncomeAlloys = false
            }
            /*AND {
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LTEQ, 1)
                }
            }*/
        }
    }

}
