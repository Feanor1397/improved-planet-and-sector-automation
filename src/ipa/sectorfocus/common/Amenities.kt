package ipa.sectorfocus.common


import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.BuildingGroup

fun SectorFocus.addCommonAmenities() {

    // amenities T1
    for (building in BuildingGroup.AmenitiesBuildingsT1) {
        addBuilding(building, priorities.base) {

            modifierAND(priorities.prebuild) {
                comment = "Pre-build when approaching threshold and not to many free jobs"
                freeAmenities(Comparators.LT, 1)
                freeJobs(Comparators.LT, 5)
            }

            modifier(priorities.urgent / priorities.prebuild) {
                comment = "Increase priority when out of amenities"
                freeAmenities(Comparators.LT, 0)
            }

            modifier(priorities.crisis / priorities.urgent) {
                comment = "Increase to crisis status if this gets out of hand"
                freeAmenities(Comparators.LT, -10)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when no workers or available or free amenities are present"
                freeJobs(Comparators.GT, 2)
                freeAmenities(Comparators.GT, 0)
            }
        }
    }

    // amenities T2
    for (building in BuildingGroup.AmenitiesBuildingsT2) {
        addBuilding(building, priorities.base) {

            // Prebuild+1 because its tier 2
            modifierAND(priorities.prebuild + 1) {
                comment = "Pre-build when approaching threshold and not to many free jobs"
                freeAmenities(Comparators.LT, 1)
                freeJobs(Comparators.LT, 5)
            }

            modifier(priorities.urgent / priorities.prebuild) {
                comment = "Increase priority when out of amenities"
                freeAmenities(Comparators.LT, 0)
            }

            modifier(priorities.crisis / priorities.urgent) {
                comment = "Increase to crisis status if this gets out of hand"
                freeAmenities(Comparators.LT, -10)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when no workers or available"
                freeJobs(Comparators.GT, 2)
                freeAmenities(Comparators.GT, 0)
            }
        }
    }

}
