package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus
import ipa.defines.BuildingGroup

fun SectorFocus.addCommonCapitals() {

    for (building in BuildingGroup.CapitalBuildings) {
        addBuilding(building, priorities.urgent, priority = true, exemptFromJobsCheck = true)
    }

}
