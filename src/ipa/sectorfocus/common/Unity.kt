package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus
import ipa.defines.Building

fun SectorFocus.addCommonUnity() {
    // addCommonUnityHive()
    addCommonUnityRegular()
    addCommonUnityMachine()
    addCommonUnitySpiritual()
}

fun SectorFocus.addCommonUnityHive() {

    addBuilding(Building.hive_node, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                addFoodIncomeBlock()
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.hive_cluster, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                addFoodIncomeBlock()
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.hive_confluence, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeRareCrystals = true
                addFoodIncomeBlock()
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

}

fun SectorFocus.addCommonUnityMachine() {

    addBuilding(Building.simulation_1, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.simulation_2, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.simulation_3, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeRareCrystals = true
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

//    addBuilding("building_alpha_hub", priorities.unity) {
//        modifierOR(priorities.disable) {
//            disableIfNoNeedForJobs()
//            controller {
//                missingIncomeEnergy = true
//                missingIncomeRareCrystals = true
//            }
//            unityAllowed(false)
//        }
//    }

}

fun SectorFocus.addCommonUnityRegular() {

    addBuilding(Building.autochthon_monument, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeConsumerGoods = true
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.heritage_site, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeConsumerGoods = true
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.hypercomms_forum, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeConsumerGoods = true
                missingIncomeRareCrystals = true
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.autocurating_vault, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeConsumerGoods = true
                missingIncomeRareCrystals = true
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

}

fun SectorFocus.addCommonUnitySpiritual() {

    addBuilding(Building.temple, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.holotemple, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.sacred_nexus, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeRareCrystals = true
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

    addBuilding(Building.citadel_of_faith, priorities.unity) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeRareCrystals = true
                upgradeMinFreeBuildingSlots()
            }
            unityAllowed(false)
        }
    }

}
