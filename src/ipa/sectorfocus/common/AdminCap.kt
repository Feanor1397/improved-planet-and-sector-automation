package ipa.sectorfocus.common

import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.BuildingGroup

fun SectorFocus.addCommonAdminCap() {

    for (building in BuildingGroup.AdminCapT1) {
        addBuilding(building, priorities.adminCap, priority = true) {
            modifierOR(priorities.disable) {
                // AND {
                //    comment = "Disallow on non-bureau worlds if owner does not have civic_byzantine_bureaucracy"
                //    NOT {
                //        owner { hasValidCivic("civic_byzantine_bureaucracy") }
                //    }
                //    hasDesignationBureau(false)
                // }
                owner {
                    comment = "Disallow if empire is below administrative cap"
                    empireSprawlCapOver(Comparators.LTEQ, 0)
                }
                disableIfNoNeedForJobs()
                adminCapAllowed(false)
                controller {
                    missingIncomeEnergy = true
                    addAdminCapRequirements()
                }
            }
        }
    }

    for (building in BuildingGroup.AdminCapT2)
        addBuilding(building, priorities.adminCap + 1, priority = true) {
            modifierOR(priorities.disable) {
                owner {
                    comment = "Disallow if empire is below administrative cap"
                    empireSprawlCapOver(Comparators.LTEQ, 0)
                }
                disableIfNoNeedForJobs()
                adminCapAllowed(false)
                upgradeMinFreeBuildingSlots()
                controller {
                    missingIncomeEnergy = true
                    addAdminCapRequirements()
                }
            }
        }

    for (building in BuildingGroup.AdminCapT3)
        addBuilding(building, priorities.adminCap + 2, priority = true) {
            modifierOR(priorities.disable) {
                owner {
                    comment = "Disallow if empire is below administrative cap"
                    empireSprawlCapOver(Comparators.LTEQ, 0)
                }
                disableIfNoNeedForJobs()
                adminCapAllowed(false)
                upgradeMinFreeBuildingSlots()
                controller {
                    missingIncomeEnergy = true
                    addAdminCapRequirements()
                }
            }
        }

}
