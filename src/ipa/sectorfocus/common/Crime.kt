package ipa.sectorfocus.common

import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.BuildingGroup

fun SectorFocus.addCommonCrime() {

    // anti-crime T1
    for (building in BuildingGroup.CrimeT1) {
        addBuilding(building, priorities.base, priority = true, exemptFromJobsCheck = true) {

            modifier(priorities.prebuild / priorities.base) {
                comment = "Pre-build when approaching threshold"
                planetCrime(Comparators.LT, 7)
            }

            modifierAND(priorities.urgent / priorities.base) {
                comment = "Increase priority when approaching threshold"
                planetCrime(Comparators.GTEQ, 8)
                planetCrime(Comparators.LT, 15)
            }

            modifier(priorities.crisis / priorities.base) {
                comment = "Increase to crisis status if this gets out of hand"
                planetCrime(Comparators.GTEQ, 15)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when no crime exists"
                planetCrime(Comparators.LT, 7)

                AND {
                    comment = "Disallow when there is a worker shortage and crime is not yet at problematic levels"
                    freeJobs(Comparators.GT, 3)
                    planetCrime(Comparators.LTEQ, 10)
                }
            }
        }
    }

}
