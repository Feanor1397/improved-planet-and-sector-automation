package ipa.sectorfocus.common

import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.Building
import ipa.defines.District

fun SectorFocus.addCommonResearch(
    onlyOnMatchingDesignation: Boolean = true,
    notOnSpecializedWorlds: Boolean = true
) {

    // research enhancers
    for (building in arrayOf(Building.supercomputer, Building.institute)) {
        addBuilding(building, priorities.enhancers, priority = true, exemptFromJobsCheck = true) {
            modifierOR(priorities.disable) {
                comment = "Disallow when not research designation or to many free jobs"
                disableIfNoNeedForJobs()

                if (onlyOnMatchingDesignation) {
                    NOR {
                        comment = "exclude non-research worlds"
                        hasDesignationResearch()
                        hasDesignationCapital()
                    }
                } else if (notOnSpecializedWorlds) {
                    OR {
                        comment = "exclude specific production worlds"
                        hasDesignationFactory()
                        hasDesignationRefinery()
                        hasDesignationFoundry()
                    }
                }

                controller {
                    missingIncomeEnergy = true
                }

                AND {
                    comment = "disallow if no research buildings"
                    numBuildings(Building.research_lab_1, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_2, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_3, Comparators.EQ, 0)
                }

                addResearchRequirements()
            }
        }
    }

    addResearchLab(
        Building.research_lab_1,
        priorities.research,
        requireExoticGasesIncome = null,
        isUpgrade = false,
        onlyOnMatchingDesignation = onlyOnMatchingDesignation,
        notOnSpecializedWorlds = notOnSpecializedWorlds
    )

    addResearchLab(
        Building.research_lab_2,
        priorities.research + 1,
        requireExoticGasesIncome = true,
        isUpgrade = true,
        onlyOnMatchingDesignation = onlyOnMatchingDesignation,
        notOnSpecializedWorlds = notOnSpecializedWorlds
    )

    addResearchLab(
        Building.research_lab_3,
        priorities.research + 2,
        requireExoticGasesIncome = true,
        isUpgrade = true,
        onlyOnMatchingDesignation = onlyOnMatchingDesignation,
        notOnSpecializedWorlds = notOnSpecializedWorlds
    )

}

private fun SectorFocus.addResearchLab(
    building: Building,
    baseWeight: Double,
    requireExoticGasesIncome: Boolean? = null,
    isUpgrade: Boolean = false,
    onlyOnMatchingDesignation: Boolean = true,
    notOnSpecializedWorlds: Boolean = true
) = addBuilding(building, baseWeight, priority = true) {
    modifierOR(priorities.disable) {
        comment = "Disallow when: not research designation, to many free jobs or not enough income"
        disableIfNoNeedForJobs()
        if (isUpgrade) {
            upgradeMinFreeBuildingSlots()
        }

        if (onlyOnMatchingDesignation) {
            NOR {
                comment = "exclude non-research worlds"
                hasDesignationResearch()
                hasDesignationCapital()
            }
        } else if (notOnSpecializedWorlds) {
            OR {
                comment = "exclude specific production worlds"
                hasDesignationFactory()
                hasDesignationRefinery()
                hasDesignationFoundry()
            }
        }

        AND {
            comment = "prevent construction of research labs while there are free research districts"
            isPlanetClassHabitat()
            numFreeDistricts(District.HabitatResearch, Comparators.GTEQ, 1)
        }

        controller {
            missingIncomeEnergy = true
            missingIncomeExoticGases = requireExoticGasesIncome
        }

        addResearchRequirements()
    }
}
