package ipa.sectorfocus.common

import ipa.blocks.sector.SectorFocus
import ipa.defines.Building

fun SectorFocus.addCommonStrategicNatural() {

    addBuilding(Building.mote_harvesters, priorities.naturalStrategic, priority = true) {
        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeVolatileMotes = true
            }
        }
        modifier(priorities.disable) {
            comment = "Disallow if to many free jobs"
            disableIfNoNeedForJobs()
        }
    }

    addBuilding(Building.gas_extractors, priorities.naturalStrategic, priority = true) {
        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeExoticGases = true
            }
        }
        modifier(priorities.disable) {
            comment = "Disallow if to many free jobs"
            disableIfNoNeedForJobs()
        }
    }

    addBuilding(Building.crystal_mines, priorities.naturalStrategic, priority = true) {
        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeRareCrystals = true
            }
        }
        modifier(priorities.disable) {
            comment = "Disallow if to many free jobs"
            disableIfNoNeedForJobs()
        }
    }

}
