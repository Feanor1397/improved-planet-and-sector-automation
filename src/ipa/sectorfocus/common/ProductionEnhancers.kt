package ipa.sectorfocus.common

import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.Building
import ipa.defines.DistrictGroup

fun SectorFocus.addCommonProductionEnhancers(
    onlyOnMatchingDesignation: Boolean = true,
    minMatchingDistricts: Int = 3
) {

    addBuilding(
        Building.mineral_purification_plant,
        priorities.enhancers,
        priority = true,
        exemptFromJobsCheck = true
    ) {
        modifierOR(priorities.disable) {
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Mining) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
            if (onlyOnMatchingDesignation) {
                hasDesignationMining(false)
            }
        }
    }
    addBuilding(
        Building.mineral_purification_hub,
        priorities.enhancers + 1,
        priority = true,
        exemptFromJobsCheck = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeVolatileMotes = true
            }
        }
    }

    addBuilding(
        Building.energy_grid,
        priorities.enhancers,
        priority = true,
        exemptFromJobsCheck = true
    ) {
        modifierOR(priorities.disable) {
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Generator) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
            if (onlyOnMatchingDesignation) {
                hasDesignationGenerator(false)
            }
        }
    }

    addBuilding(
        Building.energy_nexus,
        priorities.enhancers + 1,
        priority = true,
        exemptFromJobsCheck = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeExoticGases = true
            }
        }
    }

    addBuilding(
        Building.food_processing_facility,
        priorities.enhancers,
        priority = true,
        exemptFromJobsCheck = true
    ) {
        modifierOR(priorities.disable) {
            owner {
                usesFood = false
            }
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Farming) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
            if (onlyOnMatchingDesignation) {
                hasDesignationFarming(false)
            }
        }
    }

    addBuilding(
        Building.food_processing_center,
        priorities.enhancers + 1,
        priority = true,
        exemptFromJobsCheck = true
    ) {
        modifierOR(priorities.disable) {
            owner {
                usesFood = false
            }
            controller {
                missingIncomeVolatileMotes = true
            }
        }
    }

    addBuilding(
        Building.ministry_production,
        priorities.enhancers,
        priority = true,
        exemptFromJobsCheck = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeVolatileMotes = true
            }
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
        }
    }

    addBuilding(
        Building.production_center,
        priorities.enhancers,
        priority = true,
        exemptFromJobsCheck = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                missingIncomeVolatileMotes = true
            }
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
        }
    }

}
