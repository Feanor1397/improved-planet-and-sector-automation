package ipa.sectorfocus.common

import ipa.blocks.logic.Comparators
import ipa.blocks.sector.SectorFocus
import ipa.defines.Building
import ipa.defines.District
import ipa.defines.DistrictGroup

fun SectorFocus.addCommonFactory() {

    addBuilding(Building.factory_1, priorities.industrial) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeConsumerGoods = true
            }
        }

        modifier(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignationResearch()
            hasDesignationRefinery()
            hasDesignationFoundry()
            hasDesignationResort()
            hasDesignationLeisure()
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingDesiredIncomeConsumerGoods = false
            }
            owner {
                usesConsumerGoods = false
            }
            AND {
                numDistricts(District.Industrial, Comparators.LTEQ, 1)
            }
        }
    }

    addBuilding(Building.factory_2, priorities.industrial) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeConsumerGoods = true
            }
        }

        modifier(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingIncomeRareCrystals = true
                // always upgrade to improve efficiency, even if not missing income
                // missingDesiredIncomeConsumerGoods = false
            }
            owner {
                usesConsumerGoods = false
            }
            /*AND {
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LTEQ, 1)
                }
            }*/
        }
    }

    addBuilding(Building.factory_3, priorities.industrial) {

        modifier(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                missingIncomeConsumerGoods = true
            }
        }

        modifier(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()
            controller {
                missingIncomeEnergy = true
                missingIncomeMinerals = true
                missingIncomeRareCrystals = true
                // always upgrade to improve efficiency, even if not missing income
                // missingDesiredIncomeConsumerGoods = false
            }
            owner {
                usesConsumerGoods = false
            }
            /*AND {
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LTEQ, 1)
                }
            }*/
        }
    }

}
