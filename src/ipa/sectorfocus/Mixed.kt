package ipa.sectorfocus

import ipa.blocks.TemplateFile
import ipa.blocks.sector.SectorFocus
import ipa.sectorfocus.common.*

class Mixed : TemplateFile("common/sector_focuses/21_mixed.txt", {
    addBlock(SectorFocus("mixed")) {

        // shift priorities for mixed focus
        priorities.apply {
            districts = defaultReduced
            strategic = default
            industrial = default
            research = default
            naturalStrategic = default
            unity = default
        }

        addCommonHousing()
        addCommonUnity()
        addCommonGrowth()
        addCommonAdminCap()

        // research
        addCommonResearch(false, true)

        // district resource production
        addDistricts()

        // alloys
        addCommonFoundry()

        // consumer goods
        addCommonFactory()

        // farm buildings
        addCommonHydroponicsFarm()

        // strategic resources
        addCommonStrategicFactories()
        addCommonStrategicNatural()

        // special buildings
        addCommonProductionEnhancers(false)

        // special buildings
        addCommonMilitaryBuildings()

    }
})
