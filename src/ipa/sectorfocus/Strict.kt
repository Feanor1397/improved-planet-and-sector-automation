package ipa.sectorfocus

import ipa.blocks.TemplateFile
import ipa.blocks.sector.SectorFocus
import ipa.sectorfocus.common.*

class Strict : TemplateFile("common/sector_focuses/22_strict.txt", {
    addBlock(SectorFocus("strict")) {
        hidden = false

        addCommonHousing()
        addCommonUnity()
        addCommonGrowth()
        addCommonAdminCap()

        // research
        addCommonResearch()

        // district resource production
        addDistricts(strictDesignations = true)

        // alloys
        addCommonFoundry()

        // consumer goods
        addCommonFactory()

        // farm buildings
        addCommonHydroponicsFarm()

        // strategic resources
        addCommonStrategicFactories()
        addCommonStrategicNatural()

        // special buildings
        addCommonProductionEnhancers()

        // special buildings
        addCommonMilitaryBuildings()

    }
})
