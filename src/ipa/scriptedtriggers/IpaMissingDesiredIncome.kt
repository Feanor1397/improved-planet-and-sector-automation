package ipa.scriptedtriggers

import ipa.Policies
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger
import ipa.defines.Resource

class IpaMissingDesiredIncomeAlloys : CountryScriptedTrigger("ipa_missing_desired_income_alloys") {
    init {
        addDesiredIncome(Resource.Alloys, Variables.IncomeAlloysDesired, Policies.incomeSteps)
    }
}

class IpaMissingDesiredIncomeConsumerGoods : CountryScriptedTrigger("ipa_missing_desired_income_consumer_goods") {
    init {
        addDesiredIncome(Resource.ConsumerGoods, Variables.IncomeConsumerGoodsDesired, Policies.incomeSteps)
    }
}

class IpaMissingDesiredIncomeExoticGases : CountryScriptedTrigger("ipa_missing_desired_income_exotic_gases") {
    init {
        addDesiredIncome(Resource.ExoticGases, Variables.IncomeStrategicDesired, Policies.incomeStepsStrategic)
    }
}

class IpaMissingDesiredIncomeFood : CountryScriptedTrigger("ipa_missing_desired_income_food") {
    init {
        addDesiredIncome(Resource.Food, Variables.IncomeFoodDesired, Policies.incomeSteps)
    }
}

class IpaMissingDesiredIncomeRareCrystals : CountryScriptedTrigger("ipa_missing_desired_income_rare_crystals") {
    init {
        addDesiredIncome(Resource.RareCrystals, Variables.IncomeStrategicDesired, Policies.incomeStepsStrategic)
    }
}

class IpaMissingDesiredIncomeVolatileMotes : CountryScriptedTrigger("ipa_missing_desired_income_volatile_motes") {
    init {
        addDesiredIncome(Resource.VolatileMotes, Variables.IncomeStrategicDesired, Policies.incomeStepsStrategic)
    }
}
