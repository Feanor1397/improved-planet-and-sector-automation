package ipa.scriptedtriggers

import ipa.blocks.triggers.PlanetaryScriptedTrigger
import ipa.defines.PlanetDesignationGroup

class IpaHasDesignationCapital : PlanetaryScriptedTrigger("ipa_has_designation_capital") {
    init {
        OR {
            PlanetDesignationGroup.Capital.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationGenerator : PlanetaryScriptedTrigger("ipa_has_designation_generator") {
    init {
        OR {
            PlanetDesignationGroup.Generator.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationMining : PlanetaryScriptedTrigger("ipa_has_designation_mining") {
    init {
        OR {
            PlanetDesignationGroup.Mining.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationFarming : PlanetaryScriptedTrigger("ipa_has_designation_farming") {
    init {
        OR {
            PlanetDesignationGroup.Farming.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationResearch : PlanetaryScriptedTrigger("ipa_has_designation_research") {
    init {
        OR {
            PlanetDesignationGroup.Research.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationLeisure : PlanetaryScriptedTrigger("ipa_has_designation_leisure") {
    init {
        OR {
            PlanetDesignationGroup.Leisure.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationTrade : PlanetaryScriptedTrigger("ipa_has_designation_trade") {
    init {
        OR {
            PlanetDesignationGroup.Trade.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationUrban : PlanetaryScriptedTrigger("ipa_has_designation_urban") {
    init {
        OR {
            PlanetDesignationGroup.Urban.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationFactory : PlanetaryScriptedTrigger("ipa_has_designation_factory") {
    init {
        OR {
            PlanetDesignationGroup.Factory.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationRefinery : PlanetaryScriptedTrigger("ipa_has_designation_refinery") {
    init {
        OR {
            PlanetDesignationGroup.Refinery.forEach(::hasDesignation)
        }
    }
}


class IpaHasDesignationFoundry : PlanetaryScriptedTrigger("ipa_has_designation_foundry") {
    init {
        OR {
            PlanetDesignationGroup.Foundry.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationIndustrial : PlanetaryScriptedTrigger("ipa_has_designation_industrial") {
    init {
        OR {
            PlanetDesignationGroup.Industrial.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationFortress : PlanetaryScriptedTrigger("ipa_has_designation_fortress") {
    init {
        OR {
            PlanetDesignationGroup.Fortress.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationRural : PlanetaryScriptedTrigger("ipa_has_designation_rural") {
    init {
        OR {
            PlanetDesignationGroup.Rural.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationPenal : PlanetaryScriptedTrigger("ipa_has_designation_penal") {
    init {
        OR {
            PlanetDesignationGroup.Penal.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationResort : PlanetaryScriptedTrigger("ipa_has_designation_resort") {
    init {
        OR {
            PlanetDesignationGroup.Resort.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationMix : PlanetaryScriptedTrigger("ipa_has_designation_mix") {
    init {
        OR {
            PlanetDesignationGroup.Mix.forEach(::hasDesignation)
        }
    }
}

class IpaHasDesignationBureau : PlanetaryScriptedTrigger("ipa_has_designation_bureau") {
    init {
        OR {
            PlanetDesignationGroup.Bureau.forEach(::hasDesignation)
        }
    }
}

