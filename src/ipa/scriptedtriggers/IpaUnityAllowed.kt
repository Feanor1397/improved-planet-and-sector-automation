package ipa.scriptedtriggers

import ipa.Variables
import ipa.blocks.logic.Comparators
import ipa.blocks.triggers.PlanetaryScriptedTrigger

class IpaUnityAllowed : PlanetaryScriptedTrigger(Name) {

    companion object {
        const val Name = "ipa_unity_allowed"
    }

    init {
        OR {
            owner {
                comment = "allow unity if option is not disabled, enabled = 0, disabled = 1"
                checkVariable(Variables.OptionsDisallowUnity, Comparators.EQ, 0)
            }
        }
    }

}

