package ipa.scriptedtriggers

import ipa.Variables
import ipa.blocks.logic.Comparators
import ipa.blocks.triggers.PlanetaryScriptedTrigger

class IpaAdminCapAllowed : PlanetaryScriptedTrigger(Name) {

    companion object {
        const val Name = "ipa_admin_cap_allow"
    }

    init {
        OR {
            owner {
                comment = "if enabled without restrictions"
                checkVariable(Variables.OptionsManageAdminCap, Comparators.EQ, 0)
            }
            AND {
                comment = "if enabled and matching designation"
                owner {
                    checkVariable(Variables.OptionsManageAdminCap, Comparators.EQ, 1)
                }
                hasDesignationBureau(true)
            }
        }
    }

}

