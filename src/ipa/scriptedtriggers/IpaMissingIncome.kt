package ipa.scriptedtriggers

import ipa.Policies
import ipa.Variables
import ipa.blocks.triggers.CountryScriptedTrigger
import ipa.defines.Resource

class IpaMissingIncomeAlloys : CountryScriptedTrigger("ipa_missing_income_alloys") {
    init {
        addMinimumIncome(Resource.Alloys, Variables.IncomeAlloysMinimum, Policies.incomeSteps)
    }
}

class IpaMissingIncomeConsumerGoods : CountryScriptedTrigger("ipa_missing_income_consumer_goods") {
    init {
        addMinimumIncome(Resource.ConsumerGoods, Variables.IncomeConsumerGoodsMinimum, Policies.incomeSteps)
    }
}

class IpaMissingIncomeEnergy : CountryScriptedTrigger("ipa_missing_income_energy") {
    init {
        addMinimumIncome(Resource.Energy, Variables.IncomeEnergyMinimum, Policies.incomeSteps)
    }
}

class IpaMissingIncomeExoticGases : CountryScriptedTrigger("ipa_missing_income_exotic_gases") {
    init {
        addMinimumIncome(Resource.ExoticGases, Variables.IncomeStrategicMinimum, Policies.incomeStepsStrategic)
    }
}

class IpaMissingIncomeFood : CountryScriptedTrigger("ipa_missing_income_food") {
    init {
        addMinimumIncome(Resource.Food, Variables.IncomeFoodMinimum, Policies.incomeSteps)
    }
}

class IpaMissingIncomeMinerals : CountryScriptedTrigger("ipa_missing_income_minerals") {
    init {
        addMinimumIncome(Resource.Minerals, Variables.IncomeMineralsMinimum, Policies.incomeSteps)
    }
}

class IpaMissingIncomeRareCrystals : CountryScriptedTrigger("ipa_missing_income_rare_crystals") {
    init {
        addMinimumIncome(Resource.RareCrystals, Variables.IncomeStrategicMinimum, Policies.incomeStepsStrategic)
    }
}

class IpaMissingIncomeVolatileMotes : CountryScriptedTrigger("ipa_missing_income_volatile_motes") {
    init {
        addMinimumIncome(Resource.VolatileMotes, Variables.IncomeStrategicMinimum, Policies.incomeStepsStrategic)
    }
}
