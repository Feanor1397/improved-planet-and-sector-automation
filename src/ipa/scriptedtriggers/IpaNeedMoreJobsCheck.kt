package ipa.scriptedtriggers

import ipa.Variables
import ipa.blocks.logic.Comparators
import ipa.blocks.triggers.PlanetaryScriptedTrigger

class IpaNeedMoreJobsCheck : PlanetaryScriptedTrigger("ipa_need_more_jobs_check") {

    init {
        OR {
            // always true if missing jobs
            numUnemployed(Comparators.GTEQ, 1)

            // conditions.add("has_unemployed_or_servants = no")
            // numUnemployed(Comparators.GTEQ, 1)
            // freeJobs(Comparators.GTEQ, 0)

            for (i in 0..2) {
                AND {
                    owner {
                        checkVariable(Variables.OptionsPreBuildJobs, Comparators.EQ, i)
                    }
                    freeJobs(Comparators.LT, i)
                }
            }
        }
    }

}

