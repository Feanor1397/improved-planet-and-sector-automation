package ipa

object ModInfo {
    const val Version = "0.12.0"
    const val UpdatedAt = "2021-12-06"
}

object Policies {
    val incomeSteps = arrayOf(
        0, 1, 5, 10, 20, 30, 50, 75, 100, 150, 200, 250, 500, 750, 1000,
        1500, 2000, 3000, 5000, 10000, 15000, 20000, 30000, 40000, 50000
    )
    val incomeStepsStrategic = arrayOf(0, 1, 3, 5, 10, 20, 30, 50, 75, 100, 150, 200, 250)
}
