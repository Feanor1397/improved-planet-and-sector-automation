package ipa.blocks.logic

import ipa.Variables
import ipa.blocks.TemplateBlock
import ipa.blocks.TemplateRender

abstract class ConditionalBlock<T : ConditionalBlock<T>>(blockKey: String?) : TemplateBlock(blockKey) {

    val conditions: MutableList<String> = mutableListOf()

    /**
     * workaround: decide which combinator to use
     */
    protected fun childBlockKey(): String {
        when (blockKey) {
            "AND" -> return "AND"
            "OR" -> return "OR"
            "NOR" -> return "NOR"
            "modifier" -> return "OR"
            "trigger" -> return "OR"
            // TODO fix workaround for NOT and/or, currently only OR is supported to ensure management of alloys, consumer goods, etc work correctly
            "NOT" -> return "OR"
            else -> {
                error("invalid blockKey: $blockKey")
            }
        }
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        if (hasConditions()) {
            renderConditions(render)
        }
    }

    open fun hasConditions(): Boolean {
        return conditions.isNotEmpty()
    }

    open fun renderConditions(render: TemplateRender) {
        for (condition in conditions) {
            render.add(condition)
        }
    }

    fun childBlock(key: String, block: T.() -> Unit): T {
        val child = newChildBlock(key)
        child.block()
        children.add(child)
        return child
    }

    abstract fun newChildBlock(key: String): T;

    fun OR(block: T.() -> Unit): T {
        return childBlock("OR", block)
    }

    fun AND(block: T.() -> Unit): T {
        return childBlock("AND", block)
    }

    fun NOR(block: T.() -> Unit): T {
        return childBlock("NOR", block)
    }

    fun NOT(block: T.() -> Unit): T {
        return childBlock("NOT", block)
    }

    fun has(key: String, cmp: Comparators = Comparators.EQ, value: String = "yes") {
        conditions.add("$key $cmp $value")
    }

    fun rawCondition(condition: String) {
        conditions.add(condition)
    }

    fun hasModifier(modifier: String) {
        conditions.add("has_modifier = $modifier")
    }

    fun checkVariable(name: String, cmp: Comparators = Comparators.EQ, value: Any) {
        conditions.add("check_variable = { which = $name value $cmp $value }")
    }

    fun checkVariable(name: Variables, cmp: Comparators = Comparators.EQ, value: Any) {
        checkVariable(name.key, cmp, value)
    }

}
