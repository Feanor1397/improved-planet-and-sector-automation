package ipa.blocks.logic

import ipa.blocks.TemplateRender
import ipa.defines.Building
import ipa.defines.District
import ipa.defines.PlanetDesignation
import ipa.scriptedtriggers.IpaAdminCapAllowed
import ipa.scriptedtriggers.IpaUnityAllowed
import ipa.scriptedtriggers.IpaUpgradeMinFreeSlots
import kotlin.reflect.KMutableProperty0

abstract class RawPlanetaryConditionalBlock<T : RawPlanetaryConditionalBlock<T>>(blockKey: String?) :
    ConditionalBlock<T>(blockKey) {

    private var controller: CountryConditionalBlock? = null
    private var owner: CountryConditionalBlock? = null

    protected fun countryChildBlock(
        field: KMutableProperty0<CountryConditionalBlock?>,
        type: String,
        block: CountryConditionalBlock.() -> Unit
    ) {
        var b = field.get()
        if (b == null) {
            b = CountryConditionalBlock(type)
            b.compact = true
            field.set(b)
        }
        b.block()
    }

    fun owner(block: CountryConditionalBlock.() -> Unit) {
        countryChildBlock(::owner, "owner", block)
    }

    fun controller(block: CountryConditionalBlock.() -> Unit) {
        countryChildBlock(::controller, "controller", block)
    }

    fun hasDesignationCapital(flag: Boolean = true) {
        has("ipa_has_designation_capital", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationGenerator(flag: Boolean = true) {
        has("ipa_has_designation_generator", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationMining(flag: Boolean = true) {
        has("ipa_has_designation_mining", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationFarming(flag: Boolean = true) {
        has("ipa_has_designation_farming", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationResearch(flag: Boolean = true) {
        has("ipa_has_designation_research", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationLeisure(flag: Boolean = true) {
        has("ipa_has_designation_leisure", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationTrade(flag: Boolean = true) {
        has("ipa_has_designation_trade", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationUrban(flag: Boolean = true) {
        has("ipa_has_designation_urban", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationFactory(flag: Boolean = true) {
        has("ipa_has_designation_factory", Comparators.EQ, flag.toYesNo())
    }


    fun hasDesignationRefinery(flag: Boolean = true) {
        has("ipa_has_designation_refinery", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationFoundry(flag: Boolean = true) {
        has("ipa_has_designation_foundry", Comparators.EQ, flag.toYesNo())
    }

    /**
     * Industrial is a virtual designation, encompassing foundry and factory designations.
     */
    fun hasDesignationIndustrial(flag: Boolean = true) {
        has("ipa_has_designation_industrial", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationIndustrialAny() {
        OR {
            hasDesignationIndustrial(true)
            hasDesignationFactory(true)
            hasDesignationFoundry(true)
        }
    }

    fun notHasDesignationIndustrialAny() {
        AND {
            hasDesignationIndustrial(false)
            hasDesignationFactory(false)
            hasDesignationFoundry(false)
        }
    }

    fun hasDesignationRural(flag: Boolean = true) {
        has("ipa_has_designation_rural", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationPenal(flag: Boolean = true) {
        has("ipa_has_designation_penal", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationResort(flag: Boolean = true) {
        has("ipa_has_designation_resort", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationMix(flag: Boolean = true) {
        has("ipa_has_designation_mix", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationBureau(flag: Boolean = true) {
        has("ipa_has_designation_bureau", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignationFortress(flag: Boolean = true) {
        has("ipa_has_designation_fortress", Comparators.EQ, flag.toYesNo())
    }

    fun hasDesignation(designation: String) {
        conditions.add("has_designation = $designation")
    }

    fun hasDesignation(designation: PlanetDesignation) {
        hasDesignation(designation.code)
    }

    fun freeJobs(cmp: Comparators, value: Int) {
        // TODO investigate, might be disabled/broken!
        conditions.add("free_jobs $cmp $value")
    }

    fun numUnemployed(cmp: Comparators, value: Int) {
        conditions.add("num_unemployed $cmp $value")
    }

    fun needMoreJobsCheck(value: Boolean) {
        conditions.add("ipa_need_more_jobs_check = ${value.toYesNo()}")
    }

    fun disableIfNoNeedForJobs() {
        needMoreJobsCheck(false)
        // conditions.add("has_unemployed_or_servants = no")
        // AND {
        //     freeJobs(Comparators.GTEQ, 0)
        //     numUnemployed(Comparators.LTEQ, 0)
        // }
    }

    fun freeAmenities(cmp: Comparators, value: Int) {
        conditions.add("free_amenities $cmp $value")
    }

    fun freeHousing(cmp: Comparators, value: Int) {
        conditions.add("free_housing $cmp $value")
    }

    fun freeDistrictSlots(cmp: Comparators, value: Int) {
        conditions.add("free_district_slots $cmp $value")
    }

    fun freeBuildingSlots(cmp: Comparators, value: Int) {
        conditions.add("free_building_slots $cmp $value")
    }

    fun upgradeMinFreeBuildingSlots(flag: Boolean = true) {
        conditions.add("${IpaUpgradeMinFreeSlots.Name} = ${flag.toYesNo()}")
    }

    fun adminCapAllowed(flag: Boolean = true) {
        conditions.add("${IpaAdminCapAllowed.Name} = ${flag.toYesNo()}")
    }

    fun unityAllowed(flag: Boolean = true) {
        conditions.add("${IpaUnityAllowed.Name} = ${flag.toYesNo()}")
    }

    fun planetCrime(cmp: Comparators, value: Int) {
        conditions.add("planet_crime $cmp $value")
    }

    fun numFreeDistricts(type: String, cmp: Comparators, value: Int) {
        conditions.add("num_free_districts = { type = $type value $cmp $value }")
    }

    fun numFreeDistricts(type: District, cmp: Comparators, value: Int) {
        numFreeDistricts(type.code, cmp, value)
    }

    fun numDistricts(type: String, cmp: Comparators, value: Int) {
        conditions.add("num_districts = { type = $type value $cmp $value }")
    }

    fun numDistricts(type: District, cmp: Comparators, value: Int) {
        numDistricts(type.code, cmp, value)
    }

    fun isPlanetClass(clazz: String) {
        conditions.add("is_planet_class = $clazz")
    }

    fun isPlanetClassHabitat() {
        isPlanetClass("pc_habitat")
    }

    fun numBuildings(type: String, cmp: Comparators, value: Int) {
        conditions.add("num_buildings = { type = $type value $cmp $value }")
    }

    fun numBuildings(type: Building, cmp: Comparators, value: Int) {
        numBuildings(type.code, cmp, value)
    }

    fun addResearchRequirements() {
        AND {
            comment = "non-gestalt empires require consumer goods for research"
            owner {
                isGestalt = false
            }
            controller {
                missingIncomeConsumerGoods = true
            }
        }

        AND {
            comment = "gestalt empires require minerals for research"
            owner {
                isGestalt = true
            }
            controller {
                missingIncomeMinerals = true
            }
        }
    }

    fun addAdminCapRequirements() {
        AND {
            comment = "non-gestalt empires require consumer goods for admin cap"
            owner {
                isGestalt = false
            }
            controller {
                missingIncomeConsumerGoods = true
            }
        }
    }

    override fun hasConditions(): Boolean {
        return super.hasConditions()
            || owner?.hasConditions() == true
            || controller?.hasConditions() == true
    }

    override fun renderConditions(render: TemplateRender) {
        super.renderConditions(render)

        if (owner?.hasConditions() == true || owner?.children?.isNotEmpty() == true) {
            render.indentedBlock("owner =") {
                render.indentedBlock("${childBlockKey()} =") {
                    if (!owner!!.comment.isNullOrBlank())
                        render.add("# ${owner!!.comment}")
                    owner!!.renderConditions(render)
                    for (child in owner!!.children) {
                        child.render(render)
                    }
                }
            }
        }

        if (controller?.hasConditions() == true || controller?.children?.isNotEmpty() == true) {
            render.indentedBlock("controller =") {
                render.indentedBlock("${childBlockKey()} =") {
                    if (!controller!!.comment.isNullOrBlank())
                        render.add("# ${controller!!.comment}")
                    controller!!.renderConditions(render)
                    for (child in controller!!.children) {
                        child.render(render)
                    }
                }
            }
        }
    }

}


open class PlanetaryConditionalBlock(blockKey: String?) :
    RawPlanetaryConditionalBlock<PlanetaryConditionalBlock>(blockKey) {
    override fun newChildBlock(key: String): PlanetaryConditionalBlock {
        return PlanetaryConditionalBlock(key)
    }
}
