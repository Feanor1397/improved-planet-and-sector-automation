package ipa.blocks.logic

import ipa.blocks.TemplateRender
import ipa.defines.Resource

abstract class RawCountryConditionalBlock<T : RawCountryConditionalBlock<T>>(blockKey: String?) :
    ConditionalBlock<T>(blockKey) {

    var isGestalt: Boolean? = null
    var isLithoid: Boolean? = null
    var usesFood: Boolean? = null
    var usesConsumerGoods: Boolean? = null

    var missingIncomeMinerals: Boolean? = null
    var missingIncomeEnergy: Boolean? = null
    var missingIncomeFood: Boolean? = null
    var missingIncomeConsumerGoods: Boolean? = null
    var missingIncomeAlloys: Boolean? = null
    var missingIncomeExoticGases: Boolean? = null
    var missingIncomeVolatileMotes: Boolean? = null
    var missingIncomeRareCrystals: Boolean? = null

    var missingDesiredIncomeFood: Boolean? = null
    var missingDesiredIncomeConsumerGoods: Boolean? = null
    var missingDesiredIncomeAlloys: Boolean? = null
    var missingDesiredIncomeExoticGases: Boolean? = null
    var missingDesiredIncomeVolatileMotes: Boolean? = null
    var missingDesiredIncomeRareCrystals: Boolean? = null

    fun hasPolicyFlag(flag: String) {
        conditions.add("has_policy_flag = $flag")
    }

    fun hasMonthlyIncome(resource: String, cmp: Comparators, value: Int) {
        conditions.add("has_monthly_income = { resource = $resource value $cmp $value }")
    }

    fun hasMonthlyIncome(resource: Resource, cmp: Comparators, value: Int) {
        hasMonthlyIncome(resource.code, cmp, value)
    }

    fun hasMonthlyIncome(resource: String, cmp: Comparators, value: String) {
        conditions.add("has_monthly_income = { resource = $resource value $cmp $value }")
    }

    fun hasMonthlyIncome(resource: Resource, cmp: Comparators, value: String) {
        hasMonthlyIncome(resource.code, cmp, value)
    }

    fun allowGrowthBuilding(building: String, flag: Boolean) {
        val trigger = when (building) {
            "building_spawning_pool" -> "ipa_allow_growth_hive_spawning_pool"
            "building_robot_assembly_plant" -> "ipa_allow_growth_robot_assembly"
            "building_machine_assembly_plant" -> "ipa_allow_growth_machine_assembly"
            "building_machine_assembly_complex" -> "ipa_allow_growth_machine_assembly"
            "building_clinic" -> "ipa_allow_growth_clinic"
            "building_hospital" -> "ipa_allow_growth_hospital"
            "building_clone_vats" -> "ipa_allow_growth_clone_vats"
            "building_necrophage_elevation_chamber" -> "ipa_allow_growth_necroid_elevation"
            "building_necrophage_house_of_apotheosis" -> "ipa_allow_growth_necroid_elevation"
            else -> throw Exception("invalid growth building: $building")
        }

        conditions.add("$trigger = ${flag.toYesNo()}")
    }

    fun empireSprawlCapOver(cmp: Comparators, value: Int) {
        conditions.add("empire_sprawl_over_cap $cmp $value")
    }

    fun hasAuthority(flag: String) {
        conditions.add("has_authority = $flag")
    }

    fun hasCountryFlag(flag: String) {
        conditions.add("has_country_flag = $flag")
    }

    fun hasValidCivic(civic: String) {
        conditions.add("has_valid_civic = $civic")
    }

    override fun hasConditions(): Boolean {
        return super.hasConditions()
            || hasIncomeCondition()
            || isGestalt != null
            || isLithoid != null
            || usesFood != null
            || usesConsumerGoods != null
    }

    protected fun hasIncomeCondition(): Boolean {
        return missingIncomeEnergy != null
            || missingIncomeMinerals != null
            || missingIncomeFood != null
            || missingIncomeAlloys != null
            || missingIncomeConsumerGoods != null
            || missingIncomeRareCrystals != null
            || missingIncomeExoticGases != null
            || missingIncomeVolatileMotes != null
            || missingDesiredIncomeFood != null
            || missingDesiredIncomeConsumerGoods != null
            || missingDesiredIncomeAlloys != null
            || missingDesiredIncomeExoticGases != null
            || missingDesiredIncomeVolatileMotes != null
            || missingDesiredIncomeRareCrystals != null
    }

    override fun renderConditions(render: TemplateRender) {
        super.renderConditions(render)

        if (isGestalt != null) {
            render.add("is_gestalt = ${isGestalt!!.toYesNo()}")
        }

        if (isLithoid != null) {
            render.add("is_lithoid_empire = ${isLithoid!!.toYesNo()}")
        }

        if (usesFood != null) {
            render.add("ipa_country_uses_food = ${usesFood!!.toYesNo()}")
        }

        if (usesConsumerGoods != null) {
            render.add("country_uses_consumer_goods = ${usesConsumerGoods!!.toYesNo()}")
        }

        // --- minimum incomes
        missingIncomeEnergy?.let { render.add("ipa_missing_income_energy = ${it.toYesNo()}") }
        missingIncomeMinerals?.let { render.add("ipa_missing_income_minerals = ${it.toYesNo()}") }
        missingIncomeFood?.let { render.add("ipa_missing_income_food = ${it.toYesNo()}") }
        missingIncomeAlloys?.let { render.add("ipa_missing_income_alloys = ${it.toYesNo()}") }
        missingIncomeConsumerGoods?.let { render.add("ipa_missing_income_consumer_goods = ${it.toYesNo()}") }
        missingIncomeRareCrystals?.let { render.add("ipa_missing_income_rare_crystals = ${it.toYesNo()}") }
        missingIncomeExoticGases?.let { render.add("ipa_missing_income_exotic_gases = ${it.toYesNo()}") }
        missingIncomeVolatileMotes?.let { render.add("ipa_missing_income_volatile_motes = ${it.toYesNo()}") }

        // --- desired incomes
        missingDesiredIncomeFood?.let { render.add("ipa_missing_desired_income_food = ${it.toYesNo()}") }
        missingDesiredIncomeAlloys?.let { render.add("ipa_missing_desired_income_alloys = ${it.toYesNo()}") }
        missingDesiredIncomeConsumerGoods?.let { render.add("ipa_missing_desired_income_consumer_goods = ${it.toYesNo()}") }
        missingDesiredIncomeRareCrystals?.let { render.add("ipa_missing_desired_income_rare_crystals = ${it.toYesNo()}") }
        missingDesiredIncomeExoticGases?.let { render.add("ipa_missing_desired_income_exotic_gases = ${it.toYesNo()}") }
        missingDesiredIncomeVolatileMotes?.let { render.add("ipa_missing_desired_income_volatile_motes = ${it.toYesNo()}") }
    }

    fun addFoodIncomeBlock() {
        AND {
            isLithoid = false
            missingIncomeFood = true
        }
        AND {
            isLithoid = true
            missingIncomeMinerals = true
        }
    }

}

open class CountryConditionalBlock(blockKey: String?) : RawCountryConditionalBlock<CountryConditionalBlock>(blockKey) {

    override fun newChildBlock(key: String): CountryConditionalBlock {
        return CountryConditionalBlock(key)
    }

}

