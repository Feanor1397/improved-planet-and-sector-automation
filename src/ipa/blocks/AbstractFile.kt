package ipa.blocks

import java.io.File

abstract class AbstractFile<X : TemplateRender>(val target: String, val docTarget: String? = null) {

    var addBOM: Boolean = false

    fun writeFile(path: String) {
        val render = newTemplateRender()

        writeContent(render)

        File("$path/$target").apply {
            parentFile.mkdirs()

            writer().use {
                if (addBOM) {
                    val bom: CharArray = charArrayOf('\ufeff')
                    it.write(bom)
                }

                it.write(render.content())
            }
        }
    }

    protected abstract fun newTemplateRender(): X

    protected abstract fun writeContent(render: X)

}
