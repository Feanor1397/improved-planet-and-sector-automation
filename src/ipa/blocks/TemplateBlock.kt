package ipa.blocks

open class TemplateBlock(val blockKey: String?) : TemplateElement {

    var comment: String? = null

    val children: MutableList<TemplateElement> = mutableListOf()

    var compact: Boolean = false

    override fun render(render: TemplateRender) {
        if (!comment.isNullOrBlank())
            render.add("# $comment")

        if (!blockKey.isNullOrBlank()) {
            render.add("$blockKey = {")
            render.indent++
        }

        renderBeforeChildren(render)

        if (children.isNotEmpty()) {
            if (!compact) render.add()
            for (child in children) {
                child.render(render)
            }
        }

        renderAfterChildren(render)

        if (!blockKey.isNullOrBlank()) {
            render.indent--
            render.add("}")
        }

        if (!compact) render.add()
    }

    protected open fun renderBeforeChildren(render: TemplateRender) {

    }

    protected open fun renderAfterChildren(render: TemplateRender) {

    }

    fun <T : TemplateElement> addChild(child: T, cb: (T.() -> Unit)? = null): T {
        if (cb != null) child.cb()
        children.add(child)
        return child
    }

}
