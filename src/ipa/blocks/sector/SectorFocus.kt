package ipa.blocks.sector

import ipa.blocks.TemplateBlock
import ipa.blocks.TemplateRender
import ipa.blocks.logic.toYesNo
import ipa.defines.Building
import ipa.defines.District

open class SectorFocus(val focusName: String, block: (SectorFocus.() -> Unit)? = null) : TemplateBlock(focusName) {

    var aiWeight: Int? = 0
    var clearBlockers = true
    var hidden: Boolean? = null

    val priorities: SectorPriorities = SectorPriorities()

    init {
        comment = "Sector focus: $focusName"

        if (block != null) this.block()
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add()
        if (aiWeight != null) {
            render.indentedBlock("ai_weight =") {
                render.add("weight = $aiWeight")
            }
        }

        render.add()
        render.add("clear_blockers = ${clearBlockers.toYesNo()}")
        hidden?.let {
            render.add("hidden = ${it.toYesNo()}")
        }
    }

    fun addBuilding(
        key: Building,
        weight: Double,
        priority: Boolean? = null,
        exemptFromJobsCheck: Boolean? = null,
        block: (SectorWeight.() -> Unit)? = null
    ): SectorBuilding {
        val building = SectorBuilding(key.code)
        building.weight(weight, block)
        building.priority = priority
        building.exemptFromJobsCheck = exemptFromJobsCheck
        children.add(building)
        return building
    }

    fun addDistrict(
        key: District,
        weight: Double,
        priority: Boolean? = null,
        exemptFromJobsCheck: Boolean? = null,
        block: (SectorWeight.() -> Unit)? = null
    ): SectorDistrict {
        val district = SectorDistrict(key.code)
        district.weight(weight, block)
        district.priority = priority
        district.exemptFromJobsCheck = exemptFromJobsCheck
        children.add(district)
        return district
    }

}
