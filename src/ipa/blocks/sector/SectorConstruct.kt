package ipa.blocks.sector

import ipa.blocks.TemplateBlock
import ipa.blocks.TemplateRender
import ipa.blocks.logic.toYesNo

abstract class SectorConstruct(val constructType: String, val constructKey: String) : TemplateBlock(constructType) {

    val weight = SectorWeight(this)
    var priority: Boolean? = null
    var exemptFromJobsCheck: Boolean? = null

    init {
        comment = "$constructType: $constructKey"
        children.add(weight)
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("key = $constructKey")
        priority?.let { render.add("priority = ${it.toYesNo()}") }
        exemptFromJobsCheck?.let { render.add("exempt_from_jobs_check = ${it.toYesNo()}") }
    }

    fun weight(weight: Double, block: (SectorWeight.() -> Unit)? = null) {
        this.weight.baseWeight = weight
        if (block != null) {
            this.weight.block()
        }
    }

}
