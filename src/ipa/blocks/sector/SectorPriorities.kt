package ipa.blocks.sector


class SectorPriorities {
    val disable: Double = 0.0
    val base: Double = 1.0
    val prebuild: Double = 10.0

    val defaultReduced: Double = 99.0
    var unity: Double = 99.0

    val default: Double = 100.0
    var districts: Double = 110.0

    var adminCap: Double = 120.0

    var strategic: Double = 130.0
    var industrial: Double = 131.0
    var naturalStrategic: Double = 132.0

    var research: Double = 150.0
    var population: Double = 190.0

    var enhancers: Double = 200.0

    val urgent: Double = 300.0
    val crisis: Double = 600.0
}
