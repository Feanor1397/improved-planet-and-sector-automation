package ipa.blocks.sector

import ipa.blocks.TemplateRender
import ipa.blocks.logic.PlanetaryConditionalBlock

class SectorWeightModifier(val factor: Double) : PlanetaryConditionalBlock("modifier") {

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("factor = ${"%.2f".format(factor)}")
        super.renderBeforeChildren(render)
    }

}
