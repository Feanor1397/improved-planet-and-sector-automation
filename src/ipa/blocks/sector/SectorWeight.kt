package ipa.blocks.sector

import ipa.blocks.TemplateBlock
import ipa.blocks.TemplateRender
import ipa.blocks.logic.PlanetaryConditionalBlock

class SectorWeight(val sectorConstruct: SectorConstruct) : TemplateBlock("weight") {

    var baseWeight: Double = 1.0

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("weight = $baseWeight")
    }

    fun modifier(factor: Double, block: (PlanetaryConditionalBlock.() -> Unit)): SectorWeightModifier {
        val modifier = SectorWeightModifier(factor)
        modifier.block()
        children.add(modifier)
        return modifier
    }

    fun modifierConditional(
        factor: Double,
        combinatorCondition: String = "OR",
        block: (PlanetaryConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        val modifier = SectorWeightModifier(factor)
        val conditionalBlock = modifier.childBlock(combinatorCondition, block)

        // move immediate conditional comment up to modifier block
        modifier.comment = conditionalBlock.comment
        conditionalBlock.comment = null

        children.add(modifier)
        return modifier
    }

    fun modifierOR(
        factor: Double,
        block: (PlanetaryConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        return modifierConditional(factor, "OR", block)
    }

    fun modifierAND(
        factor: Double,
        block: (PlanetaryConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        return modifierConditional(factor, "AND", block)
    }

    fun modifierNOT(
        factor: Double,
        block: (PlanetaryConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        return modifierConditional(factor, "NOT", block)
    }

    fun modifierNOR(
        factor: Double,
        block: (PlanetaryConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        return modifierConditional(factor, "NOR", block)
    }

}
