package ipa.blocks.policies

import ipa.Variables
import ipa.blocks.TemplateBlock
import ipa.blocks.TemplateRender
import ipa.blocks.logic.Comparators
import ipa.blocks.logic.CountryConditionalBlock
import ipa.blocks.logic.toYesNo

class CountryEvent(val eventId: String, block: (CountryEvent.() -> Unit)? = null) : TemplateBlock("country_event") {

    var title: String? = null
    var desc: String? = null
    var hideWindow: Boolean = false
    var isTriggeredOnly: Boolean = true

    private var trigger: CountryConditionalBlock? = null
        get() {
            if (field == null) {
                field = CountryConditionalBlock("trigger")
                children.add(field!!)
            }
            return field!!
        }

    var options: MutableList<EventOption> = mutableListOf()

    init {
        comment = "CountryEvent: $eventId"
        if (block != null) this.block()
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("id = $eventId")

        if (!title.isNullOrBlank()) {
            render.add("title = $title")
        }

        if (!desc.isNullOrBlank()) {
            render.add("desc = $desc")
        }

        render.add("hide_window = ${hideWindow.toYesNo()}")
        render.add("is_triggered_only = ${isTriggeredOnly.toYesNo()}")

        if (options.isNotEmpty()) {
            render.add()
            for (option in options) {
                option.render(render)
            }
        }
    }

    fun trigger(block: CountryConditionalBlock.() -> Unit) {
        trigger!!.block()
    }

    fun option(name: String, block: (EventOption.() -> Unit)? = null) {
        options.add(EventOption("$eventId.$name", block))
    }

    fun addSelectOptionsForVariable(variable: Variables, options: Map<String, String>) {
        for ((optionId, optionValue) in options) {
            option(optionId) {
                trigger {
                    checkVariable(variable, Comparators.EQ, optionValue)
                }
                // return to parent
                hiddenEffect {
                    countryEvent(eventId)
                }
            }
        }
    }

    fun addToggleOption(variable: Variables) {
        val yesValue = 1
        val noValue = 0

        option(variable.key + "_yes") {
            trigger {
                checkVariable(variable, Comparators.EQ, yesValue)
            }
            hiddenEffect {
                setVariable(variable, noValue)
                countryEvent(eventId) // trigger self
            }
        }
        option(variable.key + "_no") {
            trigger {
                checkVariable(variable, Comparators.EQ, noValue)
            }
            hiddenEffect {
                setVariable(variable, yesValue)
                countryEvent(eventId) // trigger self
            }
        }
    }

}
