package ipa.blocks.policies

import ipa.blocks.TemplateBlock
import ipa.blocks.TemplateRender
import ipa.blocks.logic.CountryConditionalBlock
import ipa.blocks.logic.EffectBlock

class EventOption(val name: String, block: (EventOption.() -> Unit)? = null) : TemplateBlock("option") {

    var customTooltip: String? = null

    private var trigger: CountryConditionalBlock? = null
        get() {
            if (field == null) {
                field = CountryConditionalBlock("trigger")
                children.add(field!!)
            }
            return field!!
        }

    private var hiddenEffect: EffectBlock? = null
        get() {
            if (field == null) {
                field = EffectBlock("hidden_effect")
                children.add(field!!)
            }
            return field!!
        }

    private var immediate: EffectBlock? = null
        get() {
            if (field == null) {
                field = EffectBlock("immediate")
                children.add(field!!)
            }
            return field!!
        }

    init {
        comment = "EventOption: $name"
        if (block != null) this.block()
    }

    fun trigger(block: CountryConditionalBlock.() -> Unit) {
        trigger!!.block()
    }

    fun hiddenEffect(block: EffectBlock.() -> Unit) {
        hiddenEffect!!.block()
    }

    fun immediate(block: EffectBlock.() -> Unit) {
        immediate!!.block()
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("name = $name")
        if (!customTooltip.isNullOrEmpty())
            render.add("custom_tooltip = $customTooltip")
    }

}
