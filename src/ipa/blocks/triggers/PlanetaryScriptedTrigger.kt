package ipa.blocks.triggers

import ipa.blocks.logic.PlanetaryConditionalBlock
import ipa.blocks.logic.RawPlanetaryConditionalBlock

open class PlanetaryScriptedTrigger(triggerName: String) :
    RawPlanetaryConditionalBlock<PlanetaryConditionalBlock>(triggerName) {

    init {
        comment = "Scripted Trigger: $triggerName"
    }

    override fun newChildBlock(key: String): PlanetaryConditionalBlock {
        return PlanetaryConditionalBlock(key)
    }

}
