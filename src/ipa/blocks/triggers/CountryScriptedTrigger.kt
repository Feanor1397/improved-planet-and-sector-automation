package ipa.blocks.triggers

import ipa.Variables
import ipa.blocks.logic.Comparators
import ipa.blocks.logic.CountryConditionalBlock
import ipa.blocks.logic.RawCountryConditionalBlock
import ipa.defines.Resource

open class CountryScriptedTrigger(triggerName: String) :
    RawCountryConditionalBlock<CountryConditionalBlock>(triggerName) {

    init {
        comment = "Scripted Trigger: $triggerName"
    }

    protected fun addMinimumIncome(resource: Resource, variable: Variables, steps: Array<Int>) {
        // when (resource) {
        //     Resources.ConsumerGoods -> AND {
        //         usesConsumerGoods = true
        //         OR { addMinimumIncomeRaw(resource, variable, steps) }
        //     }
        //     else -> OR { addMinimumIncomeRaw(resource, variable, steps) }
        // }
        OR { addMinimumIncomeRaw(resource.code, variable, steps) }
    }

    private fun CountryConditionalBlock.addMinimumIncomeRaw(
        resource: String,
        variable: Variables,
        steps: Array<Int>
    ) {
        for (step in steps) {
            AND {
                checkVariable(variable, Comparators.EQ, step)
                hasMonthlyIncome(resource, Comparators.LTEQ, step)
            }
        }
    }

    protected fun addDesiredIncome(resource: Resource, variable: Variables, steps: Array<Int>) {
        // when (resource) {
        //     Resources.ConsumerGoods -> AND {
        //         usesConsumerGoods = true
        //         OR { addDesiredIncomeRaw(resource, variable, steps) }
        //     }
        //     else -> OR { addDesiredIncomeRaw(resource, variable, steps) }
        // }
        OR { addDesiredIncomeRaw(resource.code, variable, steps) }
    }

    private fun CountryConditionalBlock.addDesiredIncomeRaw(
        resource: String,
        variable: Variables,
        steps: Array<Int>
    ) {
        checkVariable(variable, Comparators.GTEQ, 10000) // unlimited
        for (step in steps) {
            AND {
                checkVariable(variable, Comparators.EQ, step)
                hasMonthlyIncome(resource, Comparators.LTEQ, step)
            }
        }
    }

    override fun newChildBlock(key: String): CountryConditionalBlock {
        return CountryConditionalBlock(key)
    }

}
