package ipa.blocks

open class TemplateFile(target: String, cb: (TemplateFile.() -> Unit)? = null) : AbstractFile<TemplateRender>(target) {

    val blocks: MutableList<TemplateBlock> = mutableListOf()
    var namespace: String? = null

    init {
        if (cb != null) cb()
    }

    fun <T : TemplateBlock> addBlock(block: T, cb: (T.() -> Unit)? = null): T {
        if (cb != null) block.cb()
        blocks.add(block)
        return block
    }

    override fun writeContent(render: TemplateRender) {
        if (namespace != null) {
            render.add("namespace = $namespace")
            render.add()
        }

        for (block in blocks) {
            block.render(render)
        }
    }

    override fun newTemplateRender(): TemplateRender {
        return TemplateRender()
    }

}
