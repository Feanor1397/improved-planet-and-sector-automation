package ipa.blocks

interface TemplateElement {

    fun render(render: TemplateRender)

}
