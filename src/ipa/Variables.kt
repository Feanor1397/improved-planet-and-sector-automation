package ipa

enum class Variables(val key: String) {
    Initialized("ipa_initialized"),

    IncomeEnergyMinimum("ipa_income_energy_minimum"),
    IncomeMineralsMinimum("ipa_income_minerals_minimum"),

    IncomeFoodMinimum("ipa_income_food_minimum"),
    IncomeFoodDesired("ipa_income_food_desired"),

    IncomeConsumerGoodsMinimum("ipa_income_consumer_goods_minimum"),
    IncomeConsumerGoodsDesired("ipa_income_consumer_goods_desired"),

    IncomeAlloysMinimum("ipa_income_alloys_minimum"),
    IncomeAlloysDesired("ipa_income_alloys_desired"),

    IncomeStrategicMinimum("ipa_income_strategic_minimum"),
    IncomeStrategicDesired("ipa_income_strategic_desired"),

    OptionsAllowUpgradeStronghold("ipa_options_allow_upgrade_strongholds"), // 0 = no, 1 = yes
    OptionsAllowAgriFarmBuildings("ipa_options_allow_agri_farm_buildings"), // 0 = no, 1 = yes
    OptionsForceShowIncomeOptions("ipa_options_force_show_income_options"), // 0 = no, 1 = yes

    OptionsAllowGrowthClinic("ipa_options_allow_growth_clinic"),
    OptionsAllowGrowthHospital("ipa_options_allow_growth_hospital"),
    OptionsAllowGrowthRobotAssembly("ipa_options_allow_growth_robot_assembly"),
    OptionsAllowGrowthCloneVats("ipa_options_allow_growth_clone_vats"),
    OptionsAllowGrowthMachineAssembly("ipa_options_allow_growth_machine_assembly"),
    OptionsAllowGrowthHiveSpawningPool("ipa_options_allow_growth_hive_spawning_pool"),
    OptionsAllowGrowthNecroidElevation("ipa_options_allow_growth_necroid_elevation"),

    OptionsUpgradeMinFreeSlots("ipa_options_upgrade_min_free_slots"), // num
    OptionsManageAdminCap("ipa_options_manage_admin_cap"), // 0 = enabled, 1 = byzantine, 2 = disabled
    OptionsDisallowUnity("ipa_options_disallow_unity"), // 0 = enabled, 1 = disabled
    OptionsPreBuildJobs("ipa_options_pre_build_jobs"), // num
}
