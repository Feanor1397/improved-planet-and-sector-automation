# Sector-based priority automation
TODO update priorities based on actual code values

- Regular Priority, Districts
	- 4: Hive District (if free housing <= and free_jobs >= 2)
	- 5: Mining District (+10 on mining world)
	- 5: Generator District (+10 on energy world)
	- 5: Farming District (+10 on farming world, if synthetic = 0)

- Default / pre-build priority
	- 10: "Pre-build something that is gonna be a problem soon", eg. amenities, not enough jobs, crime, ...

- 20-29: Advanced Resources
	- 25: Build Alloys if below desired threshold and above mineral income
	- 26: Build strategic resources if below desired threshold and above mineral income

- 30-39: Important Priority
	- 35: Research
	- 36: Monumnet / Machien Uplink / Hive Node
	- 37: Mineral/Energy/Farming Improvement Building
	- 38: Temples / robot assembly
	- 39: Population Growth buildings (for main species)

- 40: Necessities
	- 40: Housing if free < 0
	- 40: Counter Crime if > 10
	- 40: Counter Amenities if < 0

- 50: Crisis
	- 50: Counter Crime if > 15
	- 50: Counter Amenities if < -10
