name="Improved Planet and Sector Automation (Development Version)"
path="mod/improved-planet-automation-dev"
tags={
	"Buildings"
	"Economy"
	"Utilities"
	"AI"
}
supported_version="3.2.*"
